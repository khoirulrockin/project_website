<?php

use App\Http\Controllers\API\StudentController;
use App\Http\Controllers\API\StudioController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\LevelController;
use App\Http\Controllers\API\BookingController;
use App\Http\Controllers\API\PaymentController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/add-studio', [StudioController::class, 'store']);
Route::get('studios', [StudioController::class, 'index']);
Route::get('/edit-studio/{id}', [StudioController::class, 'edit']);
Route::put('update-studio/{id}', [StudioController::class, 'update']);
Route:: delete('delete-studio/{id}', [StudioController::class, 'destroy']);

Route::post('/add-category', [CategoryController::class, 'store']);
Route::get('categories', [CategoryController::class, 'index']);
Route::get('/edit-category/{id}', [CategoryController::class, 'edit']);
Route::put('update-category/{id}', [CategoryController::class, 'update']);
Route:: delete('delete-category/{id}', [CategoryController::class, 'destroy']);

Route::post('/add-admin', [AdminController::class, 'store']);
Route::get('admins', [AdminController::class, 'index']);
Route::get('/edit-admin/{id}', [AdminController::class, 'edit']);
Route::put('update-admin/{id}', [AdminController::class, 'update']);
Route:: delete('delete-admin/{id}', [AdminController::class, 'destroy']);

Route::post('/add-user', [UserController::class, 'store']);
Route::get('users', [UserController::class, 'index']);
Route::get('/edit-user/{id}', [UserController::class, 'edit']);
Route::put('update-user/{id}', [UserController::class, 'update']);
Route:: delete('delete-user/{id}', [UserController::class, 'destroy']);

Route::post('/add-level', [LevelController::class, 'store']);
Route::get('levels', [LevelController::class, 'index']);

Route::post('/add-booking', [BookingController::class, 'store']);
Route::get('bookings', [BookingController::class, 'index']);
Route::get('/edit-booking/{id}', [BookingController::class, 'edit']);
Route::put('update-booking/{id}', [BookingController::class, 'update']);
Route:: delete('delete-booking/{id}', [BookingController::class, 'destroy']);

Route::post('/add-payment', [PaymentController::class, 'store']);
Route::get('payments', [PaymentController::class, 'index']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
