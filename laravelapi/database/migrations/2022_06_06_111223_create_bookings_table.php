<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->char('id_booking', 10)->primary();
            $table->string('nik_user', 16)->index();
            $table->char('id_studio', 10)->index();
            $table->time('start_time');
            $table->time('end_time');
            $table->date('date');
            $table->char('status', 6);
            $table->char('pay', 7);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
