<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->char('id_payment', 10)->primary();
            $table->char('id_booking', 10)->index();
            $table->string('sender', 20);
            $table->char('bank', 10);
            $table->string('account_number', 20);
            $table->char('pay', 7);
            $table->date('date');
            $table->binary('proof');
            $table->string('review', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
