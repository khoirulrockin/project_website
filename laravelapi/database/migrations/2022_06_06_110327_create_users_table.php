<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('nik_user', 16)->primary();
            $table->string('name', 30);
            $table->string('gender', 12)->nullable();
            $table->string('address', 50)->nullable();
            $table->string('phone', 15)->nullable();
            $table->binary('photo')->nullable();
            $table->char('level', 1);
            $table->string('email', 30);
            $table->string('password', 32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
