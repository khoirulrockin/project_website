<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $table = 'payments';
    protected $fillable= [
        'id_payment',
        'id_booking',
        'sender',
        'bank',
        'account_number',
        'pay',
        'date',
        'proof',
        'review',
        'created_at',
        'updated_at',
    ]; 
}
