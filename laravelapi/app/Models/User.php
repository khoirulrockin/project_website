<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;
    protected $table = 'users';
    protected $fillable= [
        'nik_user',
        'name',
        'gender',
        'address',
        'phone',
        'photo',
        'level',
        'email',
        'password',
        'created_at',
        'updated_at',
    ]; 
}
