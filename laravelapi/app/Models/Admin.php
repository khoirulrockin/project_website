<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;
    protected $table = 'admins';
    protected $fillable= [
        'nik_admin',
        'name',
        'gender',
        'address',
        'phone',
        'photo',
        'level',
        'email',
        'password',
        'created_at',
        'updated_at',
    ]; 
}
