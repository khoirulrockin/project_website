<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $table = 'bookings';
    protected $fillable= [
        'id_booking',
        'nik_user',
        'id_studio',
        'start_time',
        'end_time',
        'date',
        'status',
        'pay',
        'created_at',
        'updated_at',
    ]; 
}
