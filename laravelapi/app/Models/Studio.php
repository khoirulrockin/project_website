<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Studio extends Model
{
    use HasFactory;
    protected $table = 'studios';
    protected $fillable= [
        'id_studio',
        'studio',
        'price',
        'capacity',
        'photo',
        'id_category',
        'created_at',
        'updated_at',
    ]; 
}
