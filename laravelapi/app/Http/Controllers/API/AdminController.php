<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;

class AdminController extends Controller
{
    public function index(){
        $admins = Admin::all();
        return response()->json([
            'status' =>200,
            'admins' => $admins,
        ]);

    }
    public function store(Request $request){
        $admin = new Admin;
        $admin->id = $request->input('id');
        $admin->name = $request->input('name');
        $admin->gender = $request->input('gender');
        $admin->address = $request->input('address');
        $admin->phone = $request->input('phone');
        $admin->photo = $request->input('photo');
        $admin->level = $request->input('level');
        $admin->email = $request->input('email');
        $admin->password = $request->input('password');
        $admin->created_at = $request->input('created_at');
        $admin->updated_at = $request->input('updated_at');
        $admin->save();

        return response()->json([
            'status' =>200,
            'message' => "Admin Add Succesfully",
        ]);
    }

    public function edit($id){
        $admins = Admin::find($id);
        return response()->json([
            'status'=> 200,
            'admins' => $admins,
        ]);
    }
    
    public function update(Request $request, $id){
        $admin = Admin::find($id);
        $admin->name = $request->input('name');
        $admin->gender = $request->input('gender');
        $admin->address = $request->input('address');
        $admin->phone = $request->input('phone');
        $admin->photo = $request->input('photo');
        $admin->level = $request->input('level');
        $admin->email = $request->input('email');
        $admin->password = $request->input('password');
        $admin->created_at = $request->input('created_at');
        $admin->updated_at = $request->input('updated_at');
        $admin->update();

        return response()->json([
            'status' =>200,
            'message' => "Admin Updated Succesfully",
        ]);
    }

    public function destroy($id){
        $admins = Admin::find($id);
        $admins->delete();
        return response()->json([
            'status'=> 200,
            'message' => "Admin Deleted Succesfully",
        ]);
    }
}
