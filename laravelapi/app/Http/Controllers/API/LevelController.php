<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Level;

class LevelController extends Controller
{
    public function index(){
        $levels = Level::all();
        return response()->json([
            'status' =>200,
            'levels' => $levels,
        ]);

    }
    public function store(Request $request){
        $level = new Level;
        $level->id_level = $request->input('id_level');
        $level->level = $request->input('level');
        $level->save();

        return response()->json([
            'status' =>200,
            'message' => "Level Add Succesfully",
        ]);
    }
}
