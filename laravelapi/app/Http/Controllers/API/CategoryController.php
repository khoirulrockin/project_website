<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        return response()->json([
            'status' =>200,
            'categories' => $categories,
        ]);

    }
    public function store(Request $request){
        $category = new Category;
        $category->id = $request->input('id');
        $category->type = $request->input('type');
        $category->facility = $request->input('facility');
        $category->large = $request->input('large');
        $category->created_at = $request->input('created_at');
        $category->updated_at = $request->input('updated_at');
        $category->save();

        return response()->json([
            'status' =>200,
            'message' => "Category Add Succesfully",
        ]);
    }

    public function edit($id){
        $categories = Category::find($id);
        return response()->json([
            'status'=> 200,
            'categories' => $categories,
        ]);
    }
    
    public function update(Request $request, $id){
        $category = Category::find($id);
        $category->type = $request->input('type');
        $category->facility = $request->input('facility');
        $category->large = $request->input('large');
        $category->created_at = $request->input('created_at');
        $category->updated_at = $request->input('updated_at');
        $category->update();

        return response()->json([
            'status' =>200,
            'message' => "Category Updated Succesfully",
        ]);
    }

    public function destroy($id){
        $categories = Category::find($id);
        $categories->delete();
        return response()->json([
            'status'=> 200,
            'message' => "Category Deleted Succesfully",
        ]);
    }
}
