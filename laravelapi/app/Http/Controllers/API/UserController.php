<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        return response()->json([
            'status' =>200,
            'users' => $users,
        ]);

    }
    public function store(Request $request){
        $user = new User;
        $user->nik_user = $request->input('nik_user');
        $user->name = $request->input('name');
        $user->gender = $request->input('gender');
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->photo = $request->input('photo');
        $user->level = $request->input('level');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->created_at = $request->input('created_at');
        $user->updated_at = $request->input('updated_at');
        $user->save();

        return response()->json([
            'status' =>200,
            'message' => "User Add Succesfully",
        ]);
    }
}
