<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;

class PaymentController extends Controller
{
    public function index(){
        $payments = Payment::all();
        return response()->json([
            'status' =>200,
            'payments' => $payments,
        ]);

    }
    public function store(Request $request){
        $payment = new Payment;
        $payment->id_payment = $request->input('id_payment');
        $payment->id_booking = $request->input('id_booking');
        $payment->sender = $request->input('sender');
        $payment->bank = $request->input('bank');
        $payment->account_number = $request->input('account_number');
        $payment->pay = $request->input('pay');
        $payment->date = $request->input('date');
        $payment->proof = $request->input('proof');
        $payment->review = $request->input('review');
        $payment->created_at = $request->input('created_at');
        $payment->updated_at = $request->input('updated_at');
        $payment->save();

        return response()->json([
            'status' =>200,
            'message' => "Payment Add Succesfully",
        ]);
    }
}
