<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Studio;

class StudioController extends Controller
{
    public function index(){
        $studios = Studio::all();
        return response()->json([
            'status' =>200,
            'studios' => $studios,
        ]);

    }
    public function store(Request $request){
        $studio = new Studio;
        $studio->id = $request->input('id');
        $studio->studio = $request->input('studio');
        $studio->price = $request->input('price');
        $studio->capacity = $request->input('capacity');
        $studio->photo = $request->input('photo');
        $studio->id_category = $request->input('id_category');
        $studio->created_at = $request->input('created_at');
        $studio->updated_at = $request->input('updated_at');
        $studio->save();

        return response()->json([
            'status' =>200,
            'message' => "Studio Add Succesfully",
        ]);
    }

    public function edit($id){
        $studios = Studio::find($id);
        return response()->json([
            'status'=> 200,
            'studios' => $studios,
        ]);
    }
    
    public function update(Request $request, $id){
        $studio = Studio::find($id);
        $studio->studio = $request->input('studio');
        $studio->price = $request->input('price');
        $studio->capacity = $request->input('capacity');
        $studio->photo = $request->input('photo');
        $studio->id_category = $request->input('id_category');
        $studio->created_at = $request->input('created_at');
        $studio->updated_at = $request->input('updated_at');
        $studio->update();

        return response()->json([
            'status' =>200,
            'message' => "Studio Updated Succesfully",
        ]);
    }

    public function destroy($id){
        $studios = Studio::find($id);
        $studios->delete();
        return response()->json([
            'status'=> 200,
            'message' => "Studio Deleted Succesfully",
        ]);
    }
}
