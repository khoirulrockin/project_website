<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;

class BookingController extends Controller
{
    public function index(){
        $bookings = Booking::all();
        return response()->json([
            'status' =>200,
            'bookings' => $bookings,
        ]);

    }
    public function store(Request $request){
        $booking = new Booking;
        $booking->id_booking = $request->input('id_booking');
        $booking->nik_user = $request->input('nik_user');
        $booking->id_studio = $request->input('id_studio');
        $booking->start_time = $request->input('start_time');
        $booking->end_time = $request->input('end_time');
        $booking->date = $request->input('date');
        $booking->status = $request->input('status');
        $booking->pay = $request->input('pay');
        $booking->created_at = $request->input('created_at');
        $booking->updated_at = $request->input('updated_at');
        $booking->save();

        return response()->json([
            'status' =>200,
            'message' => "Booking Add Succesfully",
        ]);
    }
}
