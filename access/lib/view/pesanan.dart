import 'package:flutter/material.dart';

class PesananPage extends StatefulWidget {
  @override
  State<PesananPage> createState() => _PesananPageState();
}

class _PesananPageState extends State<PesananPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pemesanan'),
      ),
      body: Center(
        child: const Text(
          'Pesanan Page',
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
