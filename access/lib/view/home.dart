import 'package:flutter/material.dart';
import '../constan.dart';
import 'package:flutter_project/component/bodyHome.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body:ListView.builder(
            padding: EdgeInsets.all(20),
            itemCount: 20, //data.length => bila menggunakan api
            itemBuilder: (BuildContext context, int index) {
              return Padding(padding: const EdgeInsets.only(top: 20),
              child: ListTile (leading: Image.network('https://picsum.photos/seed/251/600'),
              title: Text("Seven Studio",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,),
              subtitle: Text("Kami menyediakan berbagai background untuk foto keluarga, group, wisuda dan sebagainya",
              maxLines: 3,
              //overflow untuk memotong teks bila sudah memenuhi baris yang ditentukan
              overflow: TextOverflow.ellipsis),
              onTap: () {
                //do something
              },
              ),
              );
            },
          ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.blueGrey,
      elevation: 0,
      title: TextField(
        onTap: () {},
        readOnly: true,
        style: TextStyle(fontSize: 15),
        decoration: InputDecoration(
            hintText: 'Search',
            prefixIcon: Icon(Icons.search, color: Colors.black),
            contentPadding: const EdgeInsets.symmetric(vertical: 10.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: new BorderSide(color: Colors.white),
            ),
            fillColor: Color(0xfff3f3f4),
            filled: true),
      ),
    );
  }
}