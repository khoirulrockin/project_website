import 'package:flutter/material.dart';

class CekPesananPage extends StatefulWidget {
  @override
  State<CekPesananPage> createState() => _CekPesananPageState();
}

class _CekPesananPageState extends State<CekPesananPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cek Pemesanan'),
      ),
      body: Center(
        child: const Text(
          'Cek Pesanan Page',
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
