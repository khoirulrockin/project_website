import 'package:dio/dio.dart';
//import 'package:flutter_project/HomeScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'HomeScreen.dart';
import 'package:flutter/services.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final emailController = TextEditingController(); //untuk memasukkan isian email
  final passwordController = TextEditingController(); //untuk memasukkan isian pw
  late bool obscureText; //untuk bagian hide pw
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    obscureText = true; //true = pw diganti bintang"
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        //untuk bagian background
        body: Container(
          padding: const EdgeInsets.all(30),
          decoration: const BoxDecoration(
            image: DecorationImage(
            image: AssetImage('assets/images/bg-login.png'),
            fit: BoxFit.cover,
            ),
          ),                  
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(30),
                margin: const EdgeInsets.only(top: 25),
              ),
              const Text(
                'Welcome to',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w800,
                  fontStyle: FontStyle.normal
                ),
              ),
              const Text(
                'BS Access',
                style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                    fontFamily: "Poppins",
                    fontWeight: FontWeight.w800,
                    fontStyle: FontStyle.normal),
              ),
              const SizedBox(
                height: 80,
              ),
              Card(
                color: Colors.white.withOpacity(0.35),
                elevation: 50,
                shape: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(color: Colors.white, width: 2)
                ),
                margin: const EdgeInsets.only(bottom: 30),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(25, 50, 25, 25),
                  child: Column(
                    children: <Widget>[
                      const SizedBox(
                        height: 30,
                      ),
                      
                      //untuk inputan email
                      Container(
                          margin: const EdgeInsets.symmetric(horizontal: 10),
                          child: Column(
                            children: <Widget>[
                              Material(
                                  color: Colors.white,
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(20)),
                                  elevation: 2,
                                  child: AspectRatio(
                                      aspectRatio: 7 / 1.3,
                                      child: Center(
                                        child: TextFormField(
                                        decoration: const InputDecoration(
                                            hintText: 'Email',
                                            hintStyle: TextStyle(
                                                fontFamily: 'Poppins',
                                                fontSize: 16),
                                            border: InputBorder.none,
                                            contentPadding: EdgeInsets.all(10)),
                                        controller: emailController,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                    )
                                  )
                                )
                              ),

                              //untuk inputan pw
                              Container(
                                  margin: const EdgeInsets.only(top: 16),
                                  child: Material(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(20)),
                                      elevation: 2,
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: AspectRatio(
                                                aspectRatio: 7 / 1.3,
                                                child: Center(
                                                    child: TextFormField(
                                                  decoration:
                                                      const InputDecoration(
                                                          hintText: 'Password',
                                                          hintStyle: TextStyle(
                                                              fontFamily: 'Poppins',
                                                              fontSize: 16,),
                                                          border:
                                                              InputBorder.none,
                                                          contentPadding:
                                                              EdgeInsets.all(10)),
                                                  controller:
                                                      passwordController,
                                                  obscureText: obscureText,
                                                ))),
                                          ),
                                          //untuk ikon mata atau hide text pw
                                          IconButton(
                                            icon: Icon(
                                              Icons.remove_red_eye,
                                              color: !obscureText
                                                  ? Colors.black
                                                      .withOpacity(0.3)
                                                  : Colors.black,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                obscureText = !obscureText;
                                              });
                                            },
                                          )
                                        ],
                                      ))),
                                      //untuk inputan confirm pw
                              Container(
                                  margin: const EdgeInsets.only(top: 16),
                                  child: Material(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(20)),
                                      elevation: 2,
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: AspectRatio(
                                                aspectRatio: 7 / 1.3,
                                                child: Center(
                                                    child: TextFormField(
                                                  decoration:
                                                      const InputDecoration(
                                                          hintText: 'Confirm Password',
                                                          hintStyle: TextStyle(
                                                            fontFamily:
                                                                'Poppins',
                                                            fontSize: 16,
                                                          ),
                                                          border:
                                                              InputBorder.none,
                                                          contentPadding:
                                                              EdgeInsets.all(
                                                                  10)),
                                                  controller:
                                                      passwordController,
                                                  obscureText: obscureText,
                                                ))),
                                          ),
                                          //untuk ikon mata atau hide text pw
                                          IconButton(
                                            icon: Icon(
                                              Icons.remove_red_eye,
                                              color: !obscureText
                                                  ? Colors.black
                                                      .withOpacity(0.3)
                                                  : Colors.black,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                obscureText = !obscureText;
                                              });
                                            },
                                          )
                                        ],
                                      ))),
                              //pengaturan tombol login
                              Container(
                                  margin: const EdgeInsets.only(top: 20),
                                  width: MediaQuery.of(context).size.width,
                                  child: MaterialButton(
                                    onPressed: () {
                                      loginValidation(
                                          context); //validasi data dari field
                                    },
                                    child: Container(
                                      constraints: const BoxConstraints(
                                          minWidth: 150.0,
                                          minHeight:
                                              36.0),
                                      decoration: const BoxDecoration(
                                          gradient: LinearGradient(
                                            colors: <Color>[
                                              Color(0xFF4889EA),
                                              Color(0xFF67F3CE),
                                            ],
                                          ),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(50.0))),
                                      padding: const EdgeInsets.all(10),
                                      child: const Text("Login",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                          fontFamily: 'Poppins',
                                          fontSize: 20,
                                          fontStyle: FontStyle.normal,
                                          )
                                        ),
                                    ),
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(50))),
                                    padding: const EdgeInsets.all(10),
                                  )),
                            ],
                          )),
                        Container(
                          margin: const EdgeInsets.only(top: 4, bottom: 10),
                          width: MediaQuery.of(context).size.width,
                          child: MaterialButton(
                            onPressed: () {
                              loginValidation(
                                  context); //validasi data dari field
                            },
                            child: Container(
                              constraints: const BoxConstraints(
                                  minWidth: 150.0, minHeight: 36.0),
                              decoration: const BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: <Color>[
                                      Color(0xFF4889EA),
                                      Color(0xFF67F3CE),
                                    ],
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(50.0))),
                              padding: const EdgeInsets.all(10),
                              child: const Text("Register",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Poppins',
                                    fontSize: 20,
                                    fontStyle: FontStyle.normal,
                                  )),
                            ),
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50))),
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                          )),
                      //pengaturan tombol  sign up
                      // Container(
                      //   margin: const EdgeInsets.only(bottom: 40),
                      //   child: Column(
                      //     mainAxisAlignment: MainAxisAlignment.center,
                      //     children: [
                      //       InkWell(
                      //         onTap: () {},
                      //         child: const Text(
                      //           "Don't have an account?",
                      //           style: TextStyle(
                      //               fontSize: 16,
                      //               color: Colors.white,
                      //               fontFamily: "Poppins",
                      //               fontWeight: FontWeight.w600,
                      //               fontStyle: FontStyle.normal),
                      //         ),
                      //       ),

                      //       const SizedBox(
                      //         height: 10,
                      //       ),
                      //       // pengaturan untuk tombol lupa pw
                      //       InkWell(
                      //         onTap: () {},
                      //         child: const Text(
                      //           'Forgot Password?',
                      //           style: TextStyle(
                      //               fontSize: 16,
                      //               color: Colors.white,
                      //               fontFamily: "Poppins",
                      //               fontWeight: FontWeight.w600,
                      //               fontStyle: FontStyle.normal),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      callback();
    });
  }

//function validation untuk email dan pw
  void loginValidation(BuildContext context) async {
    bool isLoginValid = true; //sebagai indikator validasi dari email dan pw
    FocusScope.of(context).requestFocus(FocusNode());

    if (emailController.text.isEmpty) {
      isLoginValid = false;
      _onWidgetDidBuild(() {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Email Tidak Boleh Kosong'),
            backgroundColor: Colors.red,
          ),
        );
      });
    }

    if (passwordController.text.isEmpty) {
      isLoginValid = false;
      _onWidgetDidBuild(() {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Password Tidak Boleh Kosong'),
            backgroundColor: Colors.red,
          ),
        );
      });
    }
    if (isLoginValid) {
      fetchLogin(context, emailController.text, passwordController.text);
    }
  }

  fetchLogin(BuildContext context, String email, String password) async {
    final prefs = await SharedPreferences.getInstance();
    showLoaderDialog(context);
    try {
      Response response;
      var dio = Dio(); //untuk mengakses  API
      response = await dio.post(
        'https://reqres.in/api/login',
        data: {'email': email, 'password': password},
        options: Options(contentType: Headers.jsonContentType),
      );

      if (response.statusCode == 200) {
        //berhasil
        hideLoaderDialog(context);
        //setSharedPreference
        String prefEmail = email;
        String prefToken = response.data['token'];
        await prefs.setString('email', prefEmail);
        await prefs.setString('token', prefToken);
        //Messsage
        _onWidgetDidBuild(() {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Login Berhasil'),
              backgroundColor: Colors.green,
            ),
          );
        });
        //homePage
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => const Register()));
      }
    } on DioError catch (e) {
      hideLoaderDialog(context);
      if (e.response?.statusCode == 400) {
        //gagal
        String errorMessage = e.response?.data['error'];
        _onWidgetDidBuild(() {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(errorMessage),
              backgroundColor: Colors.red,
            ),
          );
        });
      } else {
        // print(e.message);
      }
    }
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: Row(
        children: [
          const CircularProgressIndicator(),
          Container(
              margin: const EdgeInsets.only(left: 7),
              child: const Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  hideLoaderDialog(BuildContext context) {
    return Navigator.pop(context);
  }
}
