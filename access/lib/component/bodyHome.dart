import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

//NOTE : listview masih belom berfungsi

class BodyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TextField(
          onTap: () {},
          readOnly: true,
          style: TextStyle(fontSize: 15),
          decoration: InputDecoration(
              hintText: 'Search',
              prefixIcon: Icon(Icons.search, color: Colors.black),
              contentPadding: const EdgeInsets.symmetric(vertical: 10.0),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: new BorderSide(color: Colors.white),
              ),
              fillColor: Color(0xfff3f3f4),
              filled: true),
        ),
        cardStudio(),
        //listStudio(),
      ],
    );
  }
}

class listStudio extends StatelessWidget{
   @override
   Widget build(BuildContext context) {
     return Container(
       child: ListView.builder(
         itemCount: 10,
         itemBuilder: (context,  index) {
           return ListTile(
             leading: Icon(Icons.music_note),
             //Text("${data[index]}") => untuk API
             title: Text("Seven Studio", maxLines: 1),
             subtitle: Text("Tempat foto di jalan xx", maxLines: 3),
           );
         }
        ),
     );
   }
}

class cardStudio extends StatelessWidget {
  const cardStudio({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
            padding: EdgeInsets.all(10),
            height: 170,
            width: 160,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.purpleAccent,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.network(
                  'https://picsum.photos/seed/251/600',
                  height: 100,
                  width: 100,
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Text(
                    " Studio Musik",
                    style:
                        TextStyle(color: Colors.black, height: 2, fontSize: 15),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          height: 170,
          width: 160,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.purpleAccent,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                'https://picsum.photos/seed/251/600',
                height: 100,
                width: 100,
              ),
              Padding(
                padding: const EdgeInsets.all(3.0),
                child: Text(
                  " Studio Foto",
                  style:
                      TextStyle(color: Colors.black, height: 2, fontSize: 15),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
