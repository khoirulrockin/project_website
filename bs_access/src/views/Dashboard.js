import React, { Component } from "react";
import { Link } from 'react-router-dom';
import '../assets/css/style.css';
import axios from 'axios';

import register from "../assets/images/register.png";
import bs_access from "../assets/images/bs-access.png";
// import login from "../assets/images/register.png";
// import logo from "../assets/images/logoW.png";
// import bus3 from "../assets/images/bus3.png";
// import bus2 from "../assets/images/bus2.png";
import data_studio_icon2 from '../assets/images/ico/icoBus_noFill.png';
import data_admin_icon2 from '../assets/images/ico/icoDriver_noFill.png';
import pemesanan_icon2 from '../assets/images/ico/icoBooking_no Fill.png';
import laporan_icon2 from '../assets/images/ico/icoReport_noFill.png';
import dashboard_icon from '../assets/images/ico/icoDash_noFill.png';
import sumber_data_icon from '../assets/images/ico/icoData_noFill.png';
import data_studio_icon from '../assets/images/ico/icoBus_noFill.png';
import data_admin_icon from '../assets/images/ico/icoDriver_noFill.png';
import data_user_icon from '../assets/images/ico/iconProfile_noFill.png';
import pemesanan_icon from '../assets/images/ico/icoBooking_no Fill.png';
import laporan_icon from '../assets/images/ico/icoReport_noFill.png';


class Dashboard extends Component{
    render() {
        return (
            <body>
                <div className="sidebar">
                    <div className="logo-details">
                        <i className="fas fa-bus">
                            <span className="logo_name">BS<span className="logo_nameMin"> Access</span></span>
                        </i>
                    </div>
                    <ul className="nav-links">
                        <li className="sidebar-heading mb-2 p-0">Menu :</li>
                        <li className="nav-item active mb-1">
                            <a href="/" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico" src={dashboard_icon} alt="logo1" data-bs-toggle="collapse"
                                        data-bs-target="#dashboard" aria-expanded="false" aria-controls="dashboard" />
                                </div>
                                <span className="link_name">Dashboard</span>
                                <i className="bx bxs-chevron-right arrow" data-bs-toggle="collapse" data-bs-target="#dashboard"
                                    aria-expanded="false" aria-controls="dashboard"></i>
                            </a>
                            <div id="dashboard" className="collapse">
                                <ul className="sub-menu">
                                    <li><a className="link_name" href="/">Dashboard</a></li>
                                    <li><a href="#">Grafik</a></li>
                                    <li><a href="#">Log</a></li>
                                    <li><a href="#">Pengaturan</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                        </li>
                        <li className="sidebar-heading mt-2 p-0">List Data</li>
                        <li className="nav-item">
                            <a href="/sumber-data" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={sumber_data_icon} alt="logo2" data-bs-toggle="collapse"
                                        data-bs-target="#SumberData" aria-expanded="false" aria-controls="SumberData" />
                                </div>
                                <span className="link_name">Source</span>
                                <i className="bx bxs-chevron-right arrow" data-bs-toggle="collapse" data-bs-target="#SumberData"
                                    aria-expanded="false" aria-controls="SumberData"></i>
                            </a>
                            <div id="SumberData" className="collapse">
                                <ul className="sub-menu">
                                    <li><a className="link_name" href="sumberData.php">Sumber Data</a></li>
                                    <li><a href="#">Terminal</a></li>
                                    <li><a href="#">Jenis Bus</a></li>
                                    <li><a href="#">Rute User</a></li>
                                    <li><a href="#">Penumpang</a></li>
                                    <li><a href="#">Staff</a></li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-item mb-1">
                            <a href="data-studio" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_studio_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data Studio</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a href="data-admin" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_admin_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data Admin</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataDriver.php">Data Driver</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a href="data-user" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_user_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data User</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataAkun.php">Data Akun</a></li>
                            </ul>
                        </li>
                        <li>
                        </li>
                        <li className="sidebar-heading mt-2 p-0">Layanan</li>
                        <li className="nav-item">
                            <a href="data-pemesanan" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={pemesanan_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Booking</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataPemesanan.php">Booking</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a href="data-laporan" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={laporan_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Report</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataLaporan.php">Report</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <div className="profile-details">
                                <div className="profile-content">
                                    <img src="https://upload.wikimedia.org/wikipedia/id/e/ea/Prince_zuko.jpg" />
                                </div>
                                <div className="name-job">
                                    <div className="profile_name">
                                    </div>
                                    <div className="job">Admin</div>
                                </div>
                                <a className="" href="logout.php"> <i className="bx bx-log-out"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="post-studio">
                    {/* CONTENT */}
                    <div className="home-section">
                        <div className="home-content d-flex justify-content-end align-items-center mb-4">
                            <div className="menu">
                                <i className="fas fa-bars"></i>
                            </div>
                            <nav className="custNav">
                                <ul className="nav">
                                    <li className="nav-item">
                                        <a href="#" className="nav-link transition">
                                            <i className="far fa-bell"></i>
                                            <span className="badge alert-danger p-1"> Staff</span>
                                        </a>
                                    </li>

                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" id="dropdownProfile" role="button" data-bs-toggle="dropdown"
                                            aria-expanded="false">
                                            <span className="RobotoReg14"></span>
                                            <img className="img-profile rounded-circle" src="https://upload.wikimedia.org/wikipedia/id/e/ea/Prince_zuko.jpg" />
                                        </a>

                                        <ul className="dropdown-menu border-0 dropdown-menu-end shadow" aria-labelledby="dropdownProfile">
                                            <li>
                                                <a className="dropdown-item" data-bs-toggle="modal"
                                                    data-bs-target="#editDataAdministrator<?php echo $sesID ?>"><i className="las la-user mr-2"></i>My
                                                    Profile</a>
                                            </li>
                                            <li>
                                                <a className="dropdown-item" href="#"> <i className="las la-list-alt mr-2"></i> Activity Log </a>
                                            </li>
                                            <li>
                                                <div className="dropdown-divider"></div>
                                            </li>
                                            <li>
                                                <a className="dropdown-item" href="logout.php"> <i className="las la-sign-out-alt mr-2"></i> Sign Out </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div className="row m-0 px-3 rowCustom">
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientBlue shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Studio</div>
                                                <div className="RobotoBold18 text-white">
                                                    <span> Studio</span></div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={data_studio_icon2} alt="logoBus" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientPink shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Admin</div>
                                                <div className="RobotoBold18 text-white">(Belum)</div>
                                            </div>
                                            <div className="col-auto">
                                            </div>
                                            {/* <img src={data_admin_icon2} className="icon-data" alt="ver" /> */}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientYellow shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Pemesanan</div>
                                                <div className="RobotoBold18 text-white">
                                                    Pe</div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={pemesanan_icon2} alt="et" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientGreen shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Total Penghasilan</div>
                                                <div className="RobotoBold18 text-white"><span>Rp.</span>
                                                    hkfdkhk</div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={laporan_icon2} alt="y" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row g-2 m-0 px-4">
                                <div class="col-lg-8">
                                    <div class="colorSecondary2 shadow roundedPanel" >
                                        
                                        <ul class="nav nav-tabs mb-3 bg-white pt-2 px-4 roundedTab custShadow" id="ex1" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tabs-1" type="button" role="tab" aria-controls="tabs-1" aria-selected="true">Dashboard</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="tab-2" data-bs-toggle="tab" data-bs-target="#tabs-2" type="button" role="tab" aria-controls="tabs-2" aria-selected="false">Grafik</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="tab-3" data-bs-toggle="tab" data-bs-target="#tabs-3" type="button" role="tab" aria-controls="tabs-3" aria-selected="false">Log</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="tab-4" data-bs-toggle="tab" data-bs-target="#tabs-4" type="button" role="tab" aria-controls="tabs-4" aria-selected="false">Pengaturan</button>
                                            </li>
                                        </ul>
                                        <div class="tab-content mb-5 px-4" id="ex1-content">
                                            <div class="tab-pane fade show active" id="tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1">
                                                <div class="responsive-map shadow">
                                                    <iframe
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3949.0431495410394!2d113.62862401436116!3d-8.198407984497482!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd69176d9e76d41%3A0x4e4c6e9a855a4fdf!2sTerminal%20Tawang%20Alun%20Jember!5e0!3m2!1sid!2sid!4v1637845956332!5m2!1sid!2sid"
                                                        width="600"
                                                        height="450"
                                                        
                                                        allowfullscreen=""
                                                        loading="lazy"
                                                    ></iframe>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tabs-2" role="tabpanel" aria-labelledby="ex1-tab-2">sfda</div>
                                            <div class="tab-pane fade" id="tabs-3" role="tabpanel" aria-labelledby="ex1-tab-3">Tab 3 content</div>
                                            <div class="tab-pane fade" id="tabs-4" role="tabpanel" aria-labelledby="ex1-tab-4">Tab 4 content</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 d-none d-lg-block">
                                    <div class="bg-white shadow text-center roundedPanel pt-3 px-4" >
                                        <span class="kalenderHead">Kalender</span>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="content w-100">
                                                    <div class="calendar-container">
                                                        <div class="calendar">
                                                            <div class="year-header">
                                                                <span class="left-button fa fa-chevron-left" id="prev"> </span>
                                                                <span class="year" id="label"></span>
                                                                <span class="right-button fa fa-chevron-right" id="next"> </span>
                                                            </div>
                                                            <table class="months-table w-100">
                                                                <tbody>
                                                                    <tr class="months-row">
                                                                        <td class="month">Jan</td>
                                                                        <td class="month">Feb</td>
                                                                        <td class="month">Mar</td>
                                                                        <td class="month">Apr</td>
                                                                        <td class="month">May</td>
                                                                        <td class="month">Jun</td>
                                                                        <td class="month">Jul</td>
                                                                        <td class="month">Aug</td>
                                                                        <td class="month">Sep</td>
                                                                        <td class="month">Oct</td>
                                                                        <td class="month">Nov</td>
                                                                        <td class="month">Dec</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                            <table class="days-table w-100">
                                                                <td class="day">Sun</td>
                                                                <td class="day">Mon</td>
                                                                <td class="day">Tue</td>
                                                                <td class="day">Wed</td>
                                                                <td class="day">Thu</td>
                                                                <td class="day">Fri</td>
                                                                <td class="day">Sat</td>
                                                            </table>
                                                            <div class="frame">
                                                                <table class="dates-table w-100">
                                                                    <tbody class="tbody"></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
        )
    }
}
export default Dashboard;