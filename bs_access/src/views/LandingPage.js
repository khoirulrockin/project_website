import React, { Component } from "react";
import { Link } from 'react-router-dom';
import '../assets/css/style.css';
import axios from 'axios';

import login from "../assets/images/register.png";
import logo from "../assets/images/bs-access-putih.png";
import bus3 from "../assets/images/bus3.png";
import bus2 from "../assets/images/bus2.png";
import landingpage from "../assets/images/index.jpg";
import landingpage1 from "../assets/images/landing-page.png";
import landingpage2 from "../assets/images/landing-page1.png";
import landingpage3 from "../assets/images/landing-page2.png";
import sally from "../assets/images/sally.png";
import aman from "../assets/images/aman.png";
import cepat from "../assets/images/cepat.png";

class LandingPage extends Component{
    render() {
        return (
            <body class="pt-0">
            <header>
                <nav class="navbar navbar-expand-md navbar-dark fixed-top landingNav">
                <div class="container">
                    <a class="navbar-brand m-0 d-flex align-self-stretch align-items-center" href="#">
                    <img src={logo} alt="Logo" id="logoNav" width="140px"/>
                    </a>
                    {/* <button class=" navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="icon fas fa-bars"></i>
                    </button> */}
                    <div class="collapse navbar-collapse justify-content-between" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto menu ps-lg-5">
                        <li class="nav-item ms-3 me-2">
                        <a class="nav-link active" href="#home">Home</a>
                        </li>
                        <li class="nav-item ms-3 me-2">
                        <a class="nav-link" href="#about">About</a>
                        </li>
                        <li class="nav-item ms-3 me-2">
                        <a class="nav-link" href="#booking">Booking</a>
                        </li>
                        <li class="nav-item ms-3 me-2">
                        <a class="nav-link" href="#contact">Contact</a>
                        </li>
                        
                    </ul>
                        <div class="ms-auto myClass">
                        {/* <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle badgeCust" href="#" id="ropdownProfile" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img class="avatar rounded-circle me-2" src="../Web Admin/fotoUser/<?php echo $sesFoto; ?>" alt="foto">
                                <span class="text-white position-relative showBadge" id="namaProfil">
                                <span class="position-absolute translate-middle badge rounded-pill colorRed show" id="badgeOrder">
                                </span>
                                </span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end myRounded border-0 shadow mod mt-2 navDrop" aria-labelledby="navbarDarkDropdownMenuLink">
                                <li><a class="dropdown-item s14" href="#" data-bs-toggle="modal" data-bs-target="#editDataAdministrator<?php echo $sesID ?>" id="editProfile">
                                    <i class="fas fa-user-edit me-2"></i>
                                    <span>Edit Profil</span>
                                </a></li>
                                <li><a class="dropdown-item s14" href="pembayaran.php" data-bs-toggle="modal" data-bs-target="#pesananSaya<?php echo $sesID ?>" id="myOrder">
                                    <i class="fas fa-receipt me-3"></i>
                                    <span class="position-relative">Pesanan saya <span class="badge colorRed" ></span></span>
                                </a></li>
                                <li>
                                <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item s14" href="logout.php">
                                    <i class="fas fa-sign-out-alt me-3"></i>
                                    <span>Logout</span>
                                </a></li>
                            </ul>
                            </li>
                        </ul> */}
                        </div>
                        <div class="ms-auto myClass">
                        <a href="/login" class="text-decoration-none">
                            <button class="btn b-cust me-2 roundedBtn text-white" id="custBtnLogin">Masuk</button>
                        </a>
                        <a href="/register" class="text-decoration-none">
                            <button class="btn roundedBtn b-cust" id="custBtnDaftar">Daftar</button>
                        </a>
                        </div>
                    
                    </div>
                </div>
                </nav>
            </header>
{/* 
            <div id="editDataAdministrator<?php echo $sesID ?>" class="modal fade">
                <div class="modal-dialog modal-lg">
                <div class="modal-content modal-edit">
                    <form role="form" action="login.php" method="POST" enctype="multipart/form-data">
                        <div class="modal-header">
                        <h4 class="modal-title">Profile</h4>
                        <button type="button" class="btn btn-danger btn-circle btn-user2 shadow" data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                            <i class="fa fa-times fa-sm"></i>
                        </button>
                        </div>
                        <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 mb-3">
                            <label for="inputId" class="form-label">NIK</label>
                            <input type="text" class="form-control form-control-user2" id="inputId" name="txt_id_user_admin" value="<?php echo $sesID ?>" placeholder="" readonly />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 mb-3">
                            <form action="editAdministrator.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="InputFotoBus" class="form-label">Foto</label>
                                <div class="img-div">
                                <div class="img-placeholder" onClick="triggerClick()">
                                    <img src="img/ico/icons8_driver_50px.png" alt="" />
                                </div>
                                <img class="img-profile rounded-circle" src="../Web Admin/fotoAdmin/<?php echo $sesFoto; ?>" onClick="triggerClick()" id="profileDisplay" />
                                <img src="img/ico/icons8_driver_50px.png" onClick="triggerClick()" id="profileDisplay" />
                                </div>
                                <input type="file" name="txt_fotoEa" onChange="displayImage(this)" id="profileImage" class="form-control"  />
                                <a href="#" class="float-end view text-secondary"> Lihat Foto </a>
                            </div>
                            </div>
                            </form>
                            <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">Nama</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="txt_nama" placeholder="Ex: Budi Santoso" required data-parsley-required-message="Data harus di isi !!!" value="<?php echo $sesName ?>" />
                            <label for="InputJenisKelamin" class="form-label">Jenis Kelamin</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="Rbtn_jenis_kelamin" id="Radios1" value="Laki-laki" checked />
                                <label class="form-label2" for="Radios1"><span>Laki-laki</span></label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="Rbtn_jenis_kelamin" id="Radios2" value="Perempuan" />
                                <label class="form-label2" for="Radios2"><span>Perempuan</span></label>
                            </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 mb-3">
                            <label for="inputAlamat" class="form-label">Tempat Lahir</label>
                            <input type="text" class="form-control form-control-user2" id="inputAlamat" name="txt_alamat" placeholder="Ex: Jl. Dharmawangsa" required data-parsley-required-message="Data harus di isi !!!" value="<?php echo $sesTempat ?>" />
                            </div>
                            <div class="col-lg-6 mb-3">
                            <label for="inputNoHp" class="form-label">Tanggal Lahir</label>
                            <input type="text" class="form-control form-control-user2" id="inputNoHp" name="txt_no_hp" placeholder="Ex: 085808241205" required data-parsley-required-message="Data harus di isi !!!" value="<?php echo $sesTanggal ?>" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 mb-3">
                            <label for="inputAlamat" class="form-label">Alamat</label>
                            <input type="text" class="form-control form-control-user2" id="inputAlamat" name="txt_alamat" placeholder="Ex: Jl. Dharmawangsa" required data-parsley-required-message="Data harus di isi !!!" value="<?php echo $sesAlamat ?>" />
                            </div>
                            <div class="col-lg-6 mb-3">
                            <label for="inputNoHp" class="form-label">No Handphone</label>
                            <input type="number" class="form-control form-control-user2" id="inputNoHp" name="txt_no_hp" placeholder="Ex: 085808241205" required data-parsley-required-message="Data harus di isi !!!" value="<?php echo $sesNoHP ?>" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 mb-3">
                            <label for="inputEmail" class="form-label">Email</label>
                            <input type="email" class="form-control form-control-user2" id="inputEmail" name="txt_email" placeholder="Ex: admin@gmail.com" required data-parsley-required-message="Data harus di isi !!!" value="<?php echo $sesEmail ?>" />
                            </div>
                            <div class="col-lg-6 mb-3">
                            <label for="inputPassword" class="form-label">Password</label>
                            <input type="password" class="form-control form-control-user2" id="inputPassword" name="txt_password" placeholder="Ex: ********" required data-parsley-required-message="Data harus di isi !!!" value="<?php echo $sesPass ?>" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 mb-3" hidden>
                            <label for="inputId" class="form-label">Status</label>
                            <input type="text" class="form-control form-control-user2" id="inputId" name="txt_id_level" value="<?php echo $sesLvl ?>" placeholder="" readonly />
                            </div>
                            <div class="col-lg-6 mb-3" hidden>
                            <label for="inputId" class="form-label">Status</label>
                            <input type="text" class="form-control form-control-user2" id="inputId" name="txt" value="User" placeholder="" readonly />
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-secondary roundedBtn" type="button" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn text-white colorPrimary roundedBtn" name="simpan">Update</button>
                        </div>
                        </div>
                    </form>
                </div>
                </div>
            </div> */}

            {/* <div id="pesananSaya<?php echo $sesID ?>" class="modal fade">
                <div class="modal-dialog modal-lg">
                <div class="modal-content modal-edit">
                    <form role="form" action="transaksi.php" method="POST" enctype="multipart/form-data">
                    <div class="modal-header">
                    <h4 class="modal-title">Pesanan Saya</h4>
                    <button type="button" class="btn btn-danger btn-circle btn-user2 shadow" data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                        <i class="fa fa-times fa-sm"></i>
                    </button>
                    </div>
                    <div class="modal-body">
                    <table class="table table-hover dataTable" width="100%">
                        <thead>
                        <tr>
                            <th class="actions">Action</th>
                            <th class="nik">ID Pemesanan </th>
                            <th>ID Bus</th>
                            <th class="nama">Waktu Pemesanan</th>
                            <th class="jk">Kursi Pesan</th>
                            <th class="no_hp">Total Bayar</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                <form action="transaksi.php" method="POST">
                                    <input hidden type="text" class="form-control form-control-user2" id="inputId" name="txt_id_pemesanan" value="<?php echo $id_pemesanan ?>" placeholder="" readonly />
                                    <input hidden type="text" class="form-control form-control-user2" id="inputId" name="txt_id_bus" value="<?php echo $id_bus ?>" placeholder="" readonly />
                                    <input hidden type="text" class="form-control form-control-user2" id="inputId" name="txt_waktu_pemesanan" value="<?php echo $waktu_pemesanan ?>" placeholder="" readonly />
                                    <input hidden type="text" class="form-control form-control-user2" id="inputId" name="txt_jumlah_kursi_pesan" value="<?php echo $kursi ?>" placeholder="" readonly />
                                    <input hidden type="text" class="form-control form-control-user2" id="inputId" name="txt_total_bayar" value="<?php echo $total ?>" placeholder="" readonly />
                                    <input hidden type="text" class="form-control form-control-user2" id="inputId" name="txt_status" value="<?php echo $status ?>" placeholder="" readonly />
                                    <a href="#" class="actionBtn" aria-label="Edit">
                                    <button type="submit" name="simpan" class="btn btn-success btn-user btn-circle" aria-label="EditModal" data-bs-toggle="modal" data-bs-target="#editDataBus<?php echo $id_bus ?>" value="edit">
                                        &nbsp;<i class="fa fa-edit fa-sm" data-bs-toggle="tooltip" title="Edit"></i>
                                    </button>
                                    </a>
                                </form>
                                <a href="hapusPemesanan.php?id_pemesanan=<?php echo $id_pemesanan; ?>" class="actionBtn" aria-label="Delete">
                                    <button class="btn btn-danger btn-user btn-circle" aria-label="DeleteModal" data-bs-toggle="modal" data-bs-target="#deleteDataBus<?php echo $id_bus ?>" value="hapus">
                                    <i class="fa fa-trash fa-sm" data-bs-toggle="tooltip" title="Delete"></i>
                                    </button>
                                </a>
                                <a class="btn btn-danger btn-user btn-circle" href="hapusPemesanan.php?id_pemesanan=<?php echo $id_pemesanan; ?>">Hapus</a>
                                </td>
                                <td>P000<?php echo $id_pemesanan; ?></td>
                                <!-- <td><?php echo $id_bus; ?></td> -->
                                <td><?php echo $waktu_pemesanan; ?></td>
                                <td><?php echo $kursi; ?> kursi</td>
                                <td><?php echo $total; ?></td>
                                <td><?php echo $status; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    </form>
                </div>
                </div>
            </div> */}

            <main>
                <section id="home">
                <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    {/* <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3" aria-label="Slide 4"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="4" aria-label="Slide 5"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="5" aria-label="Slide 6"></button>
                    </div> */}
                    <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="banner">
                            <img src={landingpage} alt="Logo" id="logoNav" width="1368px"/>
                        </div>
                        <div class="carousel-caption">
                        <h2 class="animate__animated animate__zoomIn" >Layanan Studio Foto Terbaik di Indonesia</h2>
                        <p class="animate__animated animate__fadeInUp px-0" >
                            Jasa Foto terkemuka di Indonesia dengan kualitas foto terbaik, Booking dimanapun dan kapanpun dengan mudah dan cepat !
                        </p>
                        </div>
                    </div>
                                        </div>

                    {/* <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                    </button> */}
                </div>

                <section class="search">
                    {/* <form action="pencarian.php" method="POST">
                    <div class="input-group mb-1">
                        <span class="input-group-text"><i class="fas fa-location-arrow text-white"></i></span>
                        <label for="InputIdTerminal" class="form-label"></label>
                        <select class="form-select form-select-user select-md" aria-label=".form-select-sm example" required data-parsley-required-message="Data harus di isi !!!" name="txt_pemberangkatan" >
                        <option disabled selected>-- Pilih Terminal Pemberangkatan --</option>
                            <option value="<?php echo $id_terminalst; ?>"></option>
                        
                        </select>
                        <span class="input-group-text"><i class="fas fa-exchange-alt text-white"></i></span>
                        <label for="InputIdTerminal" class="form-label"></label>
                        <select class="form-select form-select-user select-md" aria-label=".form-select-sm example" required data-parsley-required-message="Data harus di isi !!!" name="txt_tujuan" >
                        <option disabled selected>-- Pilih Terminal Tujuan --</option>
                        
                            <option value="<?php echo $id_terminalst; ?>"></option>
                        
                        </select><span class="input-group-text"><i class="fas fa-map-marker-alt text-white"></i>
                        </span>
                    </div>
                    <div class="input-group ms-auto me-auto" >
                        <span class="input-group-text"></span>
                        <input type="date" class="form-control" id="datepicker" name="txt_Tgl" required />
                        <span class="input-group-text"></span>
                    </div>
                    <div class="text-center mt-3">
                        <button type="submit" name="simpan" class="btn colorPrimary text-white roundedBtn">Cari</button>
                    </div>
                    </form> */}
                    <a href="#about" class="text-decoration-none">
                        <button class="btn colorPrimary text-white roundedBtn" id="custBtnLogin">Telusuri Kami</button>
                    </a>
                    {/* <div class="text-center mt-3">
                        <button type="submit" name="simpan" class="btn colorPrimary text-white roundedBtn">Cari</button>
                    </div> */}
                </section>
                </section>

                <section class="py-5 bg-dark" id="about">
                <div class="container py-1">
                    <div class="row myrow1 mb-0">
                    <div class="col-12 text-center p-3">
                        <h3>About Us<span class="span1"></span><span class="span2"></span></h3>
                    </div>
                    </div>
                    <div class="row myrow2 py-4">
                    <div class="col-sm-6 col-lg-4 d-flex justify-content-center mb-4">
                        <div class="card card-user1 py-4 shadow-sm hover-top" >
                        <div class="text-center step">
                            <img src={sally} alt="step1" height="200" />
                            <div class="card-body px-2">
                            <h3 class="mt-3">Praktis dan Mudah</h3>
                            <p class="mt-2 mb-0">Tinggal duduk manis, kamu bisa booking studio kapanpun yang anda inginkan.</p>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4 d-flex justify-content-center mb-3">
                        <div class="card card-user2 py-4 shadow-sm hover-top" >
                        <div class="text-center step">
                            <img src={aman} alt="step1" height="200" />
                            <div class="card-body px-2">
                            <h3 class="mt-3"><span >Aman</span></h3>
                            <p class="mt-2 mb-0 px-3">BS Access mengamankan data pribadi anda dalam sistem informasi, anda dapat menjelajah studio sepuasnya.</p>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4 d-flex justify-content-center mb-3">
                        <div class="card card-user3 py-4 shadow-sm hover-top" >
                        <div class="text-center step">
                            <img src={cepat} alt="step1" height="200"  />
                            <div class="card-body px-2">
                            <h3 class="mt-3">Cepat</h3>
                            <p class="mt-2 mb-0">BS Access solusi anda mengantri, daripada anda menunggu terlalu lama. BS Access lebih cepat untuk booking studio.</p>
                            </div>
                        </div>
                        </div>
                    </div>        
                    </div>
                </div>
                    </section>
                    
                <section class="py-5 bg-black" id="booking">
                <div class="container py-1">
                    <div class="row myrow1 py-sm-3 animate__animated animate__fadeInLeft">
                    <div class="col-sm-6 pe-sm-5 text-center text-sm-start order-sm-2">
                        <h3>BS Access solusi tepat booking studio !</h3>
                        <p class="my-4 pe-xl-8">Anda selalu antri ketika pesan studio ? </p>
                        <p class="my-4 pe-xl-8">Kami bisa membantu Anda dalam pesan studio foto yang Anda inginkan
                        dengan mudah dan cepat. Yuk cek disini !</p>
                        <a href="#"><button class="btn colorPrimary text-white d-none d-sm-inline-block">Cek <span>Studio Sekarang !</span></button>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <img class="img-fluid imgMove" src={landingpage2} alt="gbrBus" width="350px"/>
                    </div>
                    </div>
                    <div class="row myrow2 animate__animated animate__zoomIn">
                    <div class="col-12 text-center text-sm-start" >
                        <button class="btn colorPrimary roundedBtn text-white">Cek Tiket Sekarang !</button>
                    </div>
                    </div>
                </div>
                </section>

                <section class="py-5 bg-dark">
                <div class="container py-1">
                    <div class="row myrow1 py-sm-3 animate__animated animate__fadeInRight">
                    <div class="col-sm-6 ps-sm-5 text-center text-sm-start">
                        <h3>Booking studio tidak tepat waktu</h3>
                        <p class="my-4 pe-xl-8">BS Access menyediakan waktu booking yang tersedia, akan membuat anda lebih hemat waktu.</p>
                        <p class="my-4 pe-xl-8">Lakukan pemesanan studio dimanapun dan kapanpun dengan mudah dan cepat.</p>
                    </div>
                    <div class="col-sm-6 order-sm-1">
                        <img class="img-fluid imgMove" src={landingpage3} alt="gbrBus" width="350px"/>
                    </div>
                    </div>
                </div>
                </section>

                

                <section class="py-5 bg-black" id="contact">
                <div class="container py-1">
                    <div class="row myrow1 mb-4">
                    <div class="col-12 text-center p-3">
                        <h3 class="mb-5">Tentang Kami</h3>
                        <i class=""></i>
                        <p class="px-3 myPagrph"><b>BS Access</b> dikembangkan oleh mahasiswa prodi Teknik Informatika kampus
                        Politeknik Negeri Jember. </p>
                    </div>
                    </div>
                </div>
                </section>
            </main>

            {/* <section class="bg-primary-gradient text-white sectionFooter">
                <div class="bg-holder">
                </div>
                <div class="container">
                <footer class="py-5">
                    <div class="row">
                    <div class="col-12">
                        <div class="row d-flex">
                        <div class="col-4 col-md-2 order-2 order-md-1">
                            <h5 class="font-RobotoBold">Landing Page</h5>
                            <ul class="nav flex-column">
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Home</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Booking</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Help</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">About</a></li>
                            </ul>
                        </div>

                        <div class="col-4 col-md-2 order-3 order-md-2">
                            <h5 class="font-RobotoBold">Pemesanan</h5>
                            <ul class="nav flex-column">
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Pencarian tiket bus</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Pesan tiket Bus</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Pembayaran</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Cetak tiket bus</a></li>
                            <li class="nav-item mb-2">Filter</li>
                            </ul>
                        </div>

                        <div class="col-4 col-md-2 order-4 order-md-3">
                            <h5 class="font-RobotoBold">Profil</h5>
                            <ul class="nav flex-column">
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Edit Profil</a></li>
                            <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Pesanan Saya</a></li>
                            </ul>
                        </div>

                        <div class="col-12 col-md-4 offset-md-1 order-1 order-md-4 mb-5 text-center text-md-start">
                            <form>
                            <h5 class="font-RobotoBold" id="h-Footer">Bantuan !</h5>
                            <p>Jika ada masalah,hubungi kami dengan mengirimkan pesan ke email dibawah ini ! </p>
                            <div class="d-flex w-100 gap-2">
                                <label for="newsletter1" class="visually-hidden">Email address</label>
                                <input id="newsletter1" type="text" class="form-control" placeholder="Email address">
                                <button class="btn btn-primary" type="button">Hubungi</button>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="d-flex justify-content-between py-4 my-4 border-top position-relative">
                    <p>&copy; 2021 SI-BOSS Express, Inc. All rights reserved.</p>
                    <ul class="list-unstyled d-flex">
                        <li class="ms-3"><a class="link-dark" href="#">
                            <a class="text-white outline" href="#" data-bs-toggle="tooltip" data-bs-placement="left" title="Whatsapp"><i class="icofont-whatsapp s26 rounded-circle"></i></a>
                        <li class="ms-3"><a class="link-dark" href="#">
                            <a class="text-white outline" href="#" data-bs-toggle="tooltip" data-bs-placement="top" title="Instagram"><i class="icofont-instagram s22 rounded-circle"></i></a>
                        <li class="ms-3"><a class="link-dark" href="#">
                            <a class="text-white outline" href="#" data-bs-toggle="tooltip" data-bs-placement="right" title="Gmail"><i class="fa fa-google s22"></i></a>
                    </ul>
                    </div>
                </footer>
                </div>
            </section> */}

  <script src="plugin/jquery/jquery-3.6.0.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  
</body>
        )
    }
}
export default LandingPage;