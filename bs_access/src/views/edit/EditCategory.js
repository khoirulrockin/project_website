import React, { Component } from "react";
import { Link } from 'react-router-dom';
import '../../assets/css/style.css';
import axios from 'axios';

import register from "../../assets/images/register.png";
import bs_access from "../../assets/images/bs-access.png";
// import login from "../assets/images/register.png";
// import logo from "../assets/images/logoW.png";
// import bus3 from "../assets/images/bus3.png";
// import bus2 from "../assets/images/bus2.png";
import data_studio_icon2 from '../../assets/images/ico/icoBus_noFill.png';
import data_admin_icon2 from '../../assets/images/ico/icoDriver_noFill.png';
import pemesanan_icon2 from '../../assets/images/ico/icoBooking_no Fill.png';
import laporan_icon2 from '../../assets/images/ico/icoReport_noFill.png';
import dashboard_icon from '../../assets/images/ico/icoDash_noFill.png';
import sumber_data_icon from '../../assets/images/ico/icoData_noFill.png';
import data_studio_icon from '../../assets/images/ico/icoBus_noFill.png';
import data_admin_icon from '../../assets/images/ico/icoDriver_noFill.png';
import data_user_icon from '../../assets/images/ico/iconProfile_noFill.png';
import pemesanan_icon from '../../assets/images/ico/icoBooking_no Fill.png';
import laporan_icon from '../../assets/images/ico/icoReport_noFill.png';


class EditCategory extends Component{
    state = {
        id_category: '',
        type: '',
        facility: '',
        large: '',
    }

    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    async componentDidMount() {
        const category_id = this.props.match.params.id_category;
        // console.log(category_id);
        const res = await axios.get(`http://127.0.0.1:8000/api/edit-category/${category_id}`);
        if (res.data.status === 200) {
            // console.log(res.data.message);
            this.setState({
                // id_category: res.data.category.id_category,
                type: res.data.categories.type,
                facility: res.data.categories.facility,
                large: res.data.categories.large,
            });
        }

    }

    updateCategory = async (e) => {
        e.preventDefault();
        
        document.getElementById('updateBtn').disabled = true;
        document.getElementById('updateBtn').innerText = "Saving";
        const category_id = this.props.match.params.id_category;
        const res = await axios.put(`http://127.0.0.1:8000/api/update-category/${category_id}`, this.state);
        if (res.data.status === 200) {
            console.log(res.data.message);
            document.getElementById('updateBtn').disabled = false;
            document.getElementById('updateBtn').innerText = "Save"; 
        }
    }
    
    render() {
        return (
            <body>
                <div className="sidebar">
                    <div className="logo-details">
                        <i className="fas fa-bus">
                            <span className="logo_name">BS<span className="logo_nameMin"> Access</span></span>
                        </i>
                    </div>
                    <ul className="nav-links">
                        <li className="sidebar-heading mb-2 p-0">Menu :</li>
                        <li className="nav-item ">
                            <a href="/dashboard" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico" src={dashboard_icon} alt="logo1" data-bs-toggle="collapse"
                                        data-bs-target="#dashboard" aria-expanded="false" aria-controls="dashboard" />
                                </div>
                                <span className="link_name">Dashboard</span>
                                <i className="bx bxs-chevron-right arrow" data-bs-toggle="collapse" data-bs-target="#dashboard"
                                    aria-expanded="false" aria-controls="dashboard"></i>
                            </a>
                            <div id="dashboard" className="collapse">
                                <ul className="sub-menu">
                                    <li><a className="link_name" href="/dashboard">Dashboard</a></li>
                                    <li><a href="#">Grafik</a></li>
                                    <li><a href="#">Log</a></li>
                                    <li><a href="#">Pengaturan</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                        </li>
                        <li className="sidebar-heading mt-2 p-0">List Data</li>
                        <li className="nav-item active mb-1">
                            <a href="/sumber-data" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={sumber_data_icon} alt="logo2" data-bs-toggle="collapse"
                                        data-bs-target="#SumberData" aria-expanded="false" aria-controls="SumberData" />
                                </div>
                                <span className="link_name">Source</span>
                                <i className="bx bxs-chevron-right arrow" data-bs-toggle="collapse" data-bs-target="#SumberData"
                                    aria-expanded="false" aria-controls="SumberData"></i>
                            </a>
                            <div id="SumberData" className="collapse">
                                <ul className="sub-menu">
                                    <li><a className="link_name" href="sumberData.php">Sumber Data</a></li>
                                    <li><a href="#">Category</a></li>
                                    <li><a href="#">Jenis Bus</a></li>
                                    <li><a href="#">Rute User</a></li>
                                    <li><a href="#">Penumpang</a></li>
                                    <li><a href="#">Staff</a></li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-item mb-1">
                            <a href="data-studio" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_studio_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data Studio</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a href="data-admin" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_admin_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data Admin</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataDriver.php">Data Driver</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a href="data-user" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_user_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data User</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataAkun.php">Data Akun</a></li>
                            </ul>
                        </li>
                        <li>
                        </li>
                        <li className="sidebar-heading mt-2 p-0">Layanan</li>
                        <li className="nav-item">
                            <a href="data-pemesanan" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={pemesanan_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Booking</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataPemesanan.php">Booking</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a href="data-laporan" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={laporan_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Report</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataLaporan.php">Report</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <div className="profile-details">
                                <div className="profile-content">
                                    <img src="https://upload.wikimedia.org/wikipedia/id/e/ea/Prince_zuko.jpg" />
                                </div>
                                <div className="name-job">
                                    <div className="profile_name">
                                    </div>
                                    <div className="job">Admin</div>
                                </div>
                                <a className="" href="logout.php"> <i className="bx bx-log-out"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
                {/* <div className="post-studio"> */}
                    {/* CONTENT */}
                    <div className="home-section">
                        <div className="home-content d-flex justify-content-end align-items-center mb-4">
                            <div className="menu">
                                <i className="fas fa-bars"></i>
                            </div>
                            <nav className="custNav">
                                <ul className="nav">
                                    <li className="nav-item">
                                        <a href="#" className="nav-link transition">
                                            <i className="far fa-bell"></i>
                                            <span className="badge alert-danger p-1"> Staff</span>
                                        </a>
                                    </li>

                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" id="dropdownProfile" role="button" data-bs-toggle="dropdown"
                                            aria-expanded="false">
                                            <span className="RobotoReg14"></span>
                                            <img className="img-profile rounded-circle" src="https://upload.wikimedia.org/wikipedia/id/e/ea/Prince_zuko.jpg" />
                                        </a>

                                        <ul className="dropdown-menu border-0 dropdown-menu-end shadow" aria-labelledby="dropdownProfile">
                                            <li>
                                                <a className="dropdown-item" data-bs-toggle="modal"
                                                    data-bs-target="#editDataAdministrator<?php echo $sesID ?>"><i className="las la-user mr-2"></i>My
                                                    Profile</a>
                                            </li>
                                            <li>
                                                <a className="dropdown-item" href="#"> <i className="las la-list-alt mr-2"></i> Activity Log </a>
                                            </li>
                                            <li>
                                                <div className="dropdown-divider"></div>
                                            </li>
                                            <li>
                                                <a className="dropdown-item" href="logout.php"> <i className="las la-sign-out-alt mr-2"></i> Sign Out </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div className="row m-0 px-3 rowCustom">
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientBlue shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Studio</div>
                                                <div className="RobotoBold18 text-white">
                                                    <span> Studio</span></div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={data_studio_icon2} alt="logoBus" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientPink shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Admin</div>
                                                <div className="RobotoBold18 text-white">(Belum)</div>
                                            </div>
                                            <div className="col-auto">
                                            </div>
                                            {/* <img src={data_admin_icon2} className="icon-data" alt="ver" /> */}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientYellow shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Pemesanan</div>
                                                <div className="RobotoBold18 text-white">
                                                    Pe</div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={pemesanan_icon2} alt="et" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientGreen shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Total Penghasilan</div>
                                                <div className="RobotoBold18 text-white"><span>Rp.</span>
                                                    hkfdkhk</div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={laporan_icon2} alt="y" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            {/* Panel */}
                            <div class="row g-2 m-0 px-4">
                            <div class="col-12">
                                <div class="colorSecondary2 shadow roundedPanel">
                                <div className='container'>
                                </div>
                                
                                {/* <!-- Tab Category --> */}
                                <div class="tab-content mb-5" id="ex1-content">
                                    <div class="tab-pane show active" id="tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1">
                                    <div class="row g-2 m-0">
                                        <div class="col-lg-12 p-0 m-0">
                                        <div class="card mb-4 roundedTabContent">
                                            <div class="card-header shadow roundedTabContent">
                                            <div class="title float-start">
                                                <span class="m-0"><b>Category Studio Table</b></span>
                                            </div>
                                            <div class="btnAction float-end">
                                                <button class="btn btn-light text-dark btn-circle custShadow2 me-2" data-bs-toggle="modal"
                                                data-bs-target="#addData"><i class="fas fa-plus" data-bs-toggle="tooltip"
                                                    title="Tambah Data"></i></button>
                                                {/* <!-- <button class="btn btn-light text-danger btn-circle custShadow2" data-bs-toggle="modal" data-bs-target="#deleteDataCategory"><i class="fas fa-trash" data-bs-toggle="tooltip" title="Hapus Data"></i></button> --> */}
                                            </div>
                                            </div>
                                            <div class="card-body">
                                                <form role="form" onSubmit={this.updateCategory} method="POST">
                                                    <div class="modal-header">
                                                    </div>
                                                    <div class="modal-body">
                                                    {/* <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label hidden for="inputTerminal" class="form-label">ID Category</label>
                                                        <input hidden type="text" class="form-control form-control-user2" id="inputTerminal"
                                                            name="id_category" onChange={ this.handleInput} value={this.state.id_category} required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Tawang Alun"/>
                                                        </div>
                                                    </div> */}
                                                    <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputAlamat" class="form-label">Type</label>
                                                        <input type="text" class="form-control form-control-user2" id="inputAlamat"
                                                            name="type" onChange={ this.handleInput} value={this.state.type} required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Jl. Dharmawangsa" />
                                                        </div>
                                                        <div class="col-6 mb-3">
                                                        <label for="InputProvTerminal" class="form-label">Facility</label>
                                                        <input type="text" class="form-control form-control-user2" id="InputProvTerminal"
                                                            name="facility" onChange={ this.handleInput} value={this.state.facility} required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Jawa Timur" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-6 mb-3">
                                                        <label for="InputKabupatenTerminal" class="form-label">Large</label>
                                                        <input type="number" class="form-control form-control-user2" id="InputKabupatenTerminal"
                                                            name="large" onChange={ this.handleInput} value={this.state.large} required
                                                            data-parsley-required-message="Data harus di isi !!!" placeholder="Ex: Jember" />
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <input type="button" class="btn btn-secondary roundedBtn" data-bs-dismiss="modal"
                                                        value="Cancel" />
                                                        <input type="submit" id="updateBtn" name="simpan" class="btn colorPrimary text-white roundedBtn" onClick={this.updateCategory}
                                                        value="Simpan" />
                                                    </div>
                                                    </div>
                                                </form>
                                            </div>                                
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
        )
    }
}
export default EditCategory;