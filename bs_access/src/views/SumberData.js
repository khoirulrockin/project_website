import React, { Component } from "react";
import { Link } from 'react-router-dom';
import '../assets/css/style.css';
import axios from 'axios';

import register from "../assets/images/register.png";
import bs_access from "../assets/images/bs-access.png";
// import login from "../assets/images/register.png";
// import logo from "../assets/images/logoW.png";
// import bus3 from "../assets/images/bus3.png";
// import bus2 from "../assets/images/bus2.png";
import data_studio_icon2 from '../assets/images/ico/icoBus_noFill.png';
import data_admin_icon2 from '../assets/images/ico/icoDriver_noFill.png';
import pemesanan_icon2 from '../assets/images/ico/icoBooking_no Fill.png';
import laporan_icon2 from '../assets/images/ico/icoReport_noFill.png';
import dashboard_icon from '../assets/images/ico/icoDash_noFill.png';
import sumber_data_icon from '../assets/images/ico/icoData_noFill.png';
import data_studio_icon from '../assets/images/ico/icoBus_noFill.png';
import data_admin_icon from '../assets/images/ico/icoDriver_noFill.png';
import data_user_icon from '../assets/images/ico/iconProfile_noFill.png';
import pemesanan_icon from '../assets/images/ico/icoBooking_no Fill.png';
import laporan_icon from '../assets/images/ico/icoReport_noFill.png';


class SumberData extends Component{
    state = {
        id: '',
        type: '',
        facility: '',
        large: '',
    }

    state = {
        categories: [],
        loading: true,
    }

    async componentDidMount() {
        const res = await axios.get('http://127.0.0.1:8000/api/categories');
        // console.log(res);
        if (res.data.status === 200) {
            this.setState({
                categories: res.data.categories,
                loading: false,
            })
        }
    }

    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    saveCategory = async (e) => {
        e.preventDefault();
        const res = await axios.post('http://127.0.0.1:8000/api/add-category', this.state);
        if (res.data.status === 200) {
            console.log(res.data.message);
            this.setState({
                id: '',
                type: '',
                facility: '',
                large: '',
            });
        }
    }
    
    deleteCategory = async (e, id) => {
        const res = await axios.delete(`http://127.0.0.1:8000/api/delete-category/${id}`);
        if (res.data.status === 200) { 
            console.log(res.data.message);
        }
    }

    render() {
        return (
            <body>
                <div className="sidebar">
                    <div className="logo-details">
                        <i className="fas fa-bus">
                            <span className="logo_name">BS<span className="logo_nameMin"> Access</span></span>
                        </i>
                    </div>
                    <ul className="nav-links">
                        <li className="sidebar-heading mb-2 p-0">Menu :</li>
                        <li className="nav-item ">
                            <a href="/dashboard" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico" src={dashboard_icon} alt="logo1" data-bs-toggle="collapse"
                                        data-bs-target="#dashboard" aria-expanded="false" aria-controls="dashboard" />
                                </div>
                                <span className="link_name">Dashboard</span>
                                <i className="bx bxs-chevron-right arrow" data-bs-toggle="collapse" data-bs-target="#dashboard"
                                    aria-expanded="false" aria-controls="dashboard"></i>
                            </a>
                            <div id="dashboard" className="collapse">
                                <ul className="sub-menu">
                                    <li><a className="link_name" href="/dashboard">Dashboard</a></li>
                                    <li><a href="#">Grafik</a></li>
                                    <li><a href="#">Log</a></li>
                                    <li><a href="#">Pengaturan</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                        </li>
                        <li className="sidebar-heading mt-2 p-0">List Data</li>
                        <li className="nav-item active mb-1">
                            <a href="/sumber-data" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={sumber_data_icon} alt="logo2" data-bs-toggle="collapse"
                                        data-bs-target="#SumberData" aria-expanded="false" aria-controls="SumberData" />
                                </div>
                                <span className="link_name">Source</span>
                                <i className="bx bxs-chevron-right arrow" data-bs-toggle="collapse" data-bs-target="#SumberData"
                                    aria-expanded="false" aria-controls="SumberData"></i>
                            </a>
                            <div id="SumberData" className="collapse">
                                <ul className="sub-menu">
                                    <li><a className="link_name" href="sumberData.php">Sumber Data</a></li>
                                    <li><a href="#">Category</a></li>
                                    <li><a href="#">Jenis Bus</a></li>
                                    <li><a href="#">Rute User</a></li>
                                    <li><a href="#">Penumpang</a></li>
                                    <li><a href="#">Staff</a></li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-item mb-1">
                            <a href="data-studio" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_studio_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data Studio</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a href="data-admin" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_admin_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data Admin</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataDriver.php">Data Driver</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a href="data-user" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_user_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data User</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataAkun.php">Data Akun</a></li>
                            </ul>
                        </li>
                        <li>
                        </li>
                        <li className="sidebar-heading mt-2 p-0">Layanan</li>
                        <li className="nav-item">
                            <a href="data-pemesanan" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={pemesanan_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Booking</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataPemesanan.php">Booking</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a href="data-laporan" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={laporan_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Report</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataLaporan.php">Report</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <div className="profile-details">
                                <div className="profile-content">
                                    <img src="https://upload.wikimedia.org/wikipedia/id/e/ea/Prince_zuko.jpg" />
                                </div>
                                <div className="name-job">
                                    <div className="profile_name">
                                    </div>
                                    <div className="job">Admin</div>
                                </div>
                                <a className="" href="logout.php"> <i className="bx bx-log-out"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
                {/* <div className="post-studio"> */}
                    {/* CONTENT */}
                    <div className="home-section">
                        <div className="home-content d-flex justify-content-end align-items-center mb-4">
                            <div className="menu">
                                <i className="fas fa-bars"></i>
                            </div>
                            <nav className="custNav">
                                <ul className="nav">
                                    <li className="nav-item">
                                        <a href="#" className="nav-link transition">
                                            <i className="far fa-bell"></i>
                                            <span className="badge alert-danger p-1"> Staff</span>
                                        </a>
                                    </li>

                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" id="dropdownProfile" role="button" data-bs-toggle="dropdown"
                                            aria-expanded="false">
                                            <span className="RobotoReg14"></span>
                                            <img className="img-profile rounded-circle" src="https://upload.wikimedia.org/wikipedia/id/e/ea/Prince_zuko.jpg" />
                                        </a>

                                        <ul className="dropdown-menu border-0 dropdown-menu-end shadow" aria-labelledby="dropdownProfile">
                                            <li>
                                                <a className="dropdown-item" data-bs-toggle="modal"
                                                    data-bs-target="#editDataAdministrator<?php echo $sesID ?>"><i className="las la-user mr-2"></i>My
                                                    Profile</a>
                                            </li>
                                            <li>
                                                <a className="dropdown-item" href="#"> <i className="las la-list-alt mr-2"></i> Activity Log </a>
                                            </li>
                                            <li>
                                                <div className="dropdown-divider"></div>
                                            </li>
                                            <li>
                                                <a className="dropdown-item" href="logout.php"> <i className="las la-sign-out-alt mr-2"></i> Sign Out </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div className="row m-0 px-3 rowCustom">
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientBlue shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Studio</div>
                                                <div className="RobotoBold18 text-white">
                                                    <span> Studio</span></div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={data_studio_icon2} alt="logoBus" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientPink shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Admin</div>
                                                <div className="RobotoBold18 text-white">(Belum)</div>
                                            </div>
                                            <div className="col-auto">
                                            </div>
                                            {/* <img src={data_admin_icon2} className="icon-data" alt="ver" /> */}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientYellow shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Pemesanan</div>
                                                <div className="RobotoBold18 text-white">
                                                    Pe</div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={pemesanan_icon2} alt="et" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientGreen shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Total Penghasilan</div>
                                                <div className="RobotoBold18 text-white"><span>Rp.</span>
                                                    hkfdkhk</div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={laporan_icon2} alt="y" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            {/* Panel */}
                            <div class="row g-2 m-0 px-4">
                            <div class="col-12">
                                <div class="colorSecondary2 shadow roundedPanel">
                                <div className='container'>
                                </div>
                                <ul class="nav nav-tabs bg-white pt-2 px-4 roundedTab custShadow" id="ex1" role="tablist">
                                  <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tabs-1" type="button"
                                        role="tab" aria-controls="tabs-1" aria-selected="true">Category</button>
                                    </li>
                                    {/* <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tab-2" data-bs-toggle="tab" data-bs-target="#tabs-2" type="button" role="tab"
                                        aria-controls="tabs-2" aria-selected="false">Category</button>
                                    </li> */}
                                </ul>
                                {/* <!-- Tab Category --> */}
                                <div class="tab-content mb-5" id="ex1-content">
                                    <div class="tab-pane show active" id="tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1">
                                    <div class="row g-2 m-0">
                                        <div class="col-lg-12 p-0 m-0">
                                        <div class="card mb-4 roundedTabContent">
                                            <div class="card-header shadow roundedTabContent">
                                            <div class="title float-start">
                                                <span class="m-0"><b>Category Studio Table</b></span>
                                            </div>
                                            <div class="btnAction float-end">
                                                <button class="btn btn-light text-dark btn-circle custShadow2 me-2" data-bs-toggle="modal"
                                                data-bs-target="#addData"><i class="fas fa-plus" data-bs-toggle="tooltip"
                                                    title="Tambah Data"></i></button>
                                                {/* <!-- <button class="btn btn-light text-danger btn-circle custShadow2" data-bs-toggle="modal" data-bs-target="#deleteDataCategory"><i class="fas fa-trash" data-bs-toggle="tooltip" title="Hapus Data"></i></button> --> */}
                                            </div>
                                            </div>
                                            <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover dataTable nowrap" width="100%">
                                                <thead>
                                                    <tr>
                                                    <th class="cb">
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" class="selectAll" />
                                                        <label for="selectAll"></label>
                                                        </span>
                                                    </th>
                                                    <th class="actions">Action</th>
                                                    <th class="id">ID</th>
                                                    <th class="studio">Type</th>
                                                    <th class="price">Facility</th>
                                                    <th class="capacity">Large</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.categories.map((item) => {
                                                        return (
                                                            <tr key={item.id}>
                                                                <td>
                                                                    <span class="custom-checkbox">
                                                                    <input type="checkbox" id="checkbox1" name="option[]" value="<?php echo $no; ?>" />
                                                                    <label for="checkbox1"></label>
                                                                    </span>
                                                                </td>
                                                                {/* <td><a href="#" class="actionBtn" aria-label="Edit">
                                                                    <button class="btn btn-success btn-user btn-circle" aria-label="EditModal"
                                                                        data-bs-toggle="modal" data-bs-target={`#data`+`${item.id_category}`}
                                                                        value="edit">
                                                                        &nbsp;<i class="fa fa-edit fa-sm" data-bs-toggle="tooltip" title="Edit"></i>
                                                                    </button>
                                                                    </a>
                                                                    <a href="#" class="actionBtn" aria-label="Delete">
                                                                    <button class="btn btn-danger btn-user btn-circle" aria-label="DeleteModal"
                                                                        data-bs-toggle="modal" onClick={(e) => this.deleteCategory(e, item.id_category)} data-bs-target="#deleteData" value="hapus">
                                                                        <i class="fa fa-trash fa-sm" data-bs-toggle="tooltip" title="Delete"></i>
                                                                    </button>
                                                                    </a>

                                                                    <div id="{{ $item.id_category }}" class="modal fade">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content modal-edit">
                                                                        <form role="form" action="" method="POST">
                                                                            
                                                                            <div class="modal-header">
                                                                            <h4 class="modal-title">Edit Data Category</h4>
                                                                            <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                                                data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                                                <i class="fa fa-times fa-sm"></i>
                                                                            </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 mb-3" hidden>
                                                                                <label for="inputId" class="form-label">Id</label>
                                                                                <input type="text" class="form-control form-control-user2" id="inputId"
                                                                                    name="txt_id_Category" value={item.id_category} placeholder=""
                                                                                    readonly />
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-lg-6 mb-3">
                                                                                <label for="inputCategory" class="form-label">Nama Category</label>
                                                                                <input type="text" class="form-control form-control-user2"
                                                                                    id="inputCategory" name="txt_nama_Category" required
                                                                                    data-parsley-required-message="Data harus di isi !!!"
                                                                                    placeholder="Ex: Tawang Alun" value={item.type} />
                                                                                </div>
                                                                                <div class="col-lg-6 mb-3">
                                                                                <label for="inputAlamat" class="form-label">Alamat Category</label>
                                                                                <input type="text" class="form-control form-control-user2"
                                                                                    id="inputAlamat" name="txt_detail_alamat_Category"
                                                                                    placeholder="Ex: Jl. Dharmawangsa" value="<?php echo $alamat ?>"
                                                                                    required data-parsley-required-message="Data harus di isi !!!" />
                                                                                </div>
                                                                                <div class="col-12 mb-3">
                                                                                <label for="InputProvCategory" class="form-label">Provinsi</label>
                                                                                <input type="text" class="form-control form-control-user2"
                                                                                    id="InputProvCategory" name="d_provinsi_Category"
                                                                                    placeholder="Ex: Jawa Timur" required
                                                                                    data-parsley-required-message="Data harus di isi !!!"
                                                                                    value="<?php echo $provinsi ?>" />
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-6 mb-3">
                                                                                <label for="InputKabupatenCategory" class="form-label">Kabupaten</label>
                                                                                <input type="text" class="form-control form-control-user2"
                                                                                    id="InputKabupatenCategory" name="d_kabupaten_Category"
                                                                                    placeholder="Ex: Jember" required
                                                                                    data-parsley-required-message="Data harus di isi !!!"
                                                                                    value="<?php echo $kabupaten ?>" />
                                                                                </div>
                                                                                <div class="col-6 mb-3">
                                                                                <label for="InputKecamatanCategory" class="form-label">Kecamatan</label>
                                                                                <input type="text" class="form-control form-control-user2"
                                                                                    id="InputKecamatanCategory" name="d_kecamatan_Category"
                                                                                    placeholder="Ex: Rambupuji" required
                                                                                    data-parsley-required-message="Data harus di isi !!!"
                                                                                    value="<?php echo $kecamatan ?>" />
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button class="btn btn-secondary roundedBtn" type="button"
                                                                                data-bs-dismiss="modal">Batal</button>
                                                                                <button type="submit" class="btn text-white colorPrimary roundedBtn"
                                                                                name="simpan">Update</button>
                                                                            </div>
                                                                            </div>
                                                                        </form>
                                                                        </div>
                                                                    </div>
                                                                    </div>

                                                                    <div id="deleteDataCategory<?php echo $id_Category; ?>" class="modal fade">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                        <form action="">
                                                                            <div class="modal-header">
                                                                            <h4 class="modal-title">Hapus Category</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                                aria-label="Close" aria-hidden="true"></button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                            <p>Apakah Anda yakin ingin menghapus data Category ini ?</p>
                                                                            <p class="text-warning"><small>Perlu hati-hati karena data akan hilang
                                                                                selamanya !</small></p>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                            <button class="btn btn-secondary" type="button"
                                                                                data-bs-dismiss="modal">Batal</button>
                                                                            <a class="btn btn-danger"
                                                                                href="hapusCategory.php?id_Category=<?php echo $id_Category; ?>">Hapus</a>
                                                                            </div>
                                                                        </form>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                </td> */}
                                                                <td>
                                                                    <Link to={`/edit-category/${item.id}`} class="btn btn-success btn-user btn-circle" ></Link>
                                                                    <button class="btn btn-danger btn-user btn-circle" aria-label="DeleteModal"
                                                                        data-bs-toggle="modal"data-bs-target="#deleteData" value="hapus" onClick={(e) => this.deleteCategory(e, item.id)}>
                                                                        <i class="fa fa-trash fa-sm" data-bs-toggle="tooltip" title="Delete"></i>
                                                                    </button>
                                                                </td>
                                                                <td>{item.id}</td>
                                                                <td>{item.type}</td>
                                                                <td>{item.facility}</td>
                                                                <td>{item.large}</td>
                                                            </tr>
                                                        );
                                                    })}
                                                </tbody>
                                                </table>
                                            </div>
                                            </div>

                                            {/* <!-- Tambah Modal --> */}
                                            <div id="addData" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content modal-edit">
                                                <form role="form" onSubmit={this.saveCategory} method="POST">
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Add Data Category</h4>
                                                    <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                        data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                        <i class="fa fa-times fa-sm"></i>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputCategory" class="form-label">ID Category</label>
                                                        <input type="text" class="form-control form-control-user2" id="inputCategory"
                                                            name="id" onChange={ this.handleInput} value={this.state.id} required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Tawang Alun" />
                                                        </div>
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputAlamat" class="form-label">Type</label>
                                                        <input type="text" class="form-control form-control-user2" id="inputAlamat"
                                                            name="type" onChange={ this.handleInput} value={this.state.type} required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Jl. Dharmawangsa" />
                                                        </div>
                                                    </div>

                                                        <div class="row">
                                                        <div class="col-6 mb-3">
                                                        <label for="InputProvCategory" class="form-label">Facility</label>
                                                        <input type="text" class="form-control form-control-user2" id="InputProvCategory"
                                                            name="facility" onChange={ this.handleInput} value={this.state.facility} required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Jawa Timur" />
                                                        </div>
                                                        <div class="col-6 mb-3">
                                                        <label for="InputKabupatenCategory" class="form-label">Large</label>
                                                        <input type="number" class="form-control form-control-user2" id="InputKabupatenCategory"
                                                            name="large" onChange={ this.handleInput} value={this.state.large} required
                                                            data-parsley-required-message="Data harus di isi !!!" placeholder="Ex: Jember" />
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="modal-footer">
                                                        <input type="button" class="btn btn-secondary roundedBtn" data-bs-dismiss="modal"
                                                        value="Cancel" />
                                                        <input type="submit" name="simpan" class="btn colorPrimary text-white roundedBtn" onClick={this.saveCategory}
                                                        value="Simpan" />
                                                    </div>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
        )
    }
}
export default SumberData;