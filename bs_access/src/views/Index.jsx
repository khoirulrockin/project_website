import React, { Component } from "react";
// import Register from "./Register/Register";
import Login from "./Login";

class Index extends Component {
    state = {
        bs_access: [],
        insertStudio: {
            id: 1,
            studio: "",
            address: "",
            description: "",
            phone: "",
            price: 1
        }
    }

    getAPI = () => {
        fetch('http://localhost:3001/studio?_sort=id&_order=desc')
            .then(response => response.json())
            .then(resultAPI => {
                this.setState({
                    bs_access: resultAPI
                })
            })
    }

    componentDidMount() {
        this.getAPI()
    }

    delete_studio = (data) => {
        fetch(`http://localhost:3001/studio/${data}`, { method: 'DELETE' })
            .then(res => {
                this.getAPI()
            })
    }

    insertStudio = (event) => {
        let formInsertStudio = { ...this.state.insertStudio };
        let timestamp = new Date().getTime();
        formInsertStudio['id'] = timestamp;
        formInsertStudio[event.target.name] = event.target.value;
        this.setState({
            insertStudio: formInsertStudio
        });
    }

    button_save = () => {
        fetch('http://localhost:3001/studio/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.insertStudio)
        })
            .then((Response) => {
                this.getAPI();
            })
    }

    render() {
        return (
            <div className="post-studio">
                {/* <Register /> */}
                <Login/>
            </div>
        )
    }
}

export default Index;