import React, { Component } from "react";
import { Link } from 'react-router-dom';
import '../assets/css/style.css';
import axios from 'axios';


import register from "../assets/images/register.png";
import bs_access from "../assets/images/bs-access.png";
// import login from "../assets/images/register.png";
// import logo from "../assets/images/logoW.png";
// import bus3 from "../assets/images/bus3.png";
// import bus2 from "../assets/images/bus2.png";


class Register extends Component{
    render() {
        return (
            <body class="bg-white">
                <div class="info-data" data-infodata="<?php if(isset($_SESSION['info'])){ echo $_SESSION['info']; } unset($_SESSION['info']); ?>"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="card o-hidden border-0 shadow-lg">
                                <div class="card-body p-0">
                                    <div class="row autoHeight">
                                        <div class="col-lg-4 d-none d-lg-flex justify-content-center colorPrimary">
                                            <div class="banner py-5 position-fixed">
                                                <h3 class="mb-6">
                                                    System Information Booking <br />
                                                    Studio Access <br /><span>Safe, Easy, and Fast</span>
                                                </h3>
                                                <img src={register} alt="logo_bus" class="img-fluid" width="376px" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8 p-4 py-3 colorSecondary bg-img">
                                            <div class="logoT">
                                                <a>
                                                    {/* <img src={bs_access} alt="LogoBSAccess" /> */}
                                                    {/* <h3>BS Access</h3> */}
                                                </a>
                                            </div>
                                            <div class="panelFormDaftar o-hidden border-0 shadow">
                                                <div class="p-5">
                                                    <div class="judul">
                                                        <h4 class="text-gray-900 mb-5">Register <br /><span>System Information Booking Studio Access</span>
                                                        </h4>
                                                    </div>
                                                    <form class="custom-validation" action="function.php" method="POST">
                                                        <div class="col-lg-12 mb-3" hidden>
                                                            <label for="InputId" class="form-label">Id</label>
                                                            <input type="text" class="form-control form-control-user2" id="InputId" name="txt_id"
                                                                placeholder="" />
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 mb-3">
                                                                <label for="InputNama" class="form-label">Full Name</label>
                                                                <input type="text" class="form-control form-control-user2" id="InputNama" name="txt_nama"
                                                                    required data-parsley-required-message="Nama lengkap harus di isi !!!"
                                                                    placeholder="Ex: Muhammad Khoirul Rosikin" />
                                                            </div>
                                                            <div class="col-lg-6 mb-3">
                                                                <label for="InputEmail" class="form-label">Email</label>
                                                                <input type="email" class="form-control form-control-user2" id="InputEmail" name="txt_email"
                                                                    required data-parsley-required-message="Email harus di isi !!!"
                                                                    placeholder="Ex: mkhoirulr97@gmail.com" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-6 mb-3">
                                                                <label for="password-input" class="form-label">Password</label>
                                                                <input type="password" class="form-control form-control-user2" id="password-input"
                                                                    name="txt_password" required data-parsley-required-message="Kata sandi harus di isi !!!"
                                                                    placeholder="********" data-parsley-length="[8,16]" maxlength="16" data-parsley-length-message="Harus disiisi 8 sampai 16 karakter !!!"
                                                                    data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1" />
                                                            </div>
                                                            <div class="col-lg-6 mb-3">
                                                                <label for="Kpassword-input" class="form-label">Confirm Password</label>
                                                                <input type="password" class="form-control form-control-user2" id="Kpassword-input"
                                                                    name="txt_pass" required data-parsley-required-message="Masukan ulang kata sandi !!!"
                                                                    data-parsley-equalto="#password-input" data-parsley-equalto-message="Kata sandi tidak cocok" placeholder="********" />
                                                                {/* <span class="s10 p-0 mb-2 d-flex justify-content-end show-hideWithText mt-1" toggle="#password-input" toggle2="#Kpassword-input" id="spanShow">
                                                                    Tampilkan Kata Sandi
                                                                </span> */}
                                                            </div>

                                                        </div>
                                                        <input type="hidden" name="id_level"/>
                                                            <div class="row">
                                                                <div class="col-lg-6 mb-3">
                                                                    <label for="InputNoHp" class="form-label">Phone</label>
                                                                    <input type="text" class="form-control form-control-user2" id="InputNoHp" name="txt_no_hp"
                                                                        required data-parsley-required-message="No. HP harus di isi !!!"
                                                                        placeholder="Ex: 085808241204" />
                                                                </div>
                                                                <div class="col-lg-6 mb-3">
                                                                    <label for="InputJenisKelamin" class="form-label">Gender</label>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" name="Rbtn_jenis_kelamin" id="Radios1"
                                                                            value="Laki-laki" checked />
                                                                        <label class="form-label2" for="Radios1"><span>Male</span></label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" name="Rbtn_jenis_kelamin" id="Radios2"
                                                                            value="Perempuan" />
                                                                        <label class="form-label2" for="Radios2"><span>Female</span></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 mb-3">
                                                                    <label for="InputAlamat" class="form-label">Address</label>
                                                                    <textarea type="text" class="form-control form-control-user2" id="InputAlamat" name="txt_alamat"
                                                                        required data-parsley-required-message="Alamat harus di isi !!!"
                                                                        placeholder="Ex: Jl. KH Ahmad Dahlan, RT. 002, RW. 005, Dusun. Tambakrejo" />
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                            <div class="mb-5"></div>
                                                            <div class="col-12 d-flex justify-content-center mb-3">
                                                            <a href="/ogin" class="btn colorSecondary btn-login text-white btn-block2">
                                                                <span>Register</span>
                                                            </a>
                                                            </div>
                                                            <div class="col-12 d-flex justify-content-center">
                                                                <a href="/login" class="btn btn-daftar btn-block2 py-2">
                                                                    <span>Login</span>
                                                                </a>
                                                            </div>
                                                            <div class="mb-3"></div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="d-block position-fixed markQuestion2">
                                            </div>
                                            <footer class="d-flex justify-content-center text-center">
                                                <p class="col-md-4 mb-0 text-muted">&copy; 2022 BS Access, All Rights Reserved</p>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="TambahDataTerminal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content modal-edit">
                            <form action="tambahTerminalRegister.php" method="POST">
                                <div class="modal-header">
                                    <h4 class="modal-title">Tambah Data Terminal</h4>
                                    <button type="button" class="btn btn-danger btn-circle btn-user2 shadow" data-bs-dismiss="modal"
                                        aria-label="Close" aria-hidden="true">
                                        <i class="fa fa-times fa-sm"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 mb-3" hidden>
                                        <label for="InputId" class="form-label">Id</label>
                                        <input type="text" class="form-control form-control-user2" id="InputId" name="txt_id_terminal"
                                            placeholder="" />
                                    </div>
                                    <div class="row">
                                        <div class="col-12 mb-3">
                                            <label for="InputNamaTerminal" class="form-label">Nama Terminal</label>
                                            <input type="text" class="form-control form-control-user2" id="InputNamaTerminal"
                                                name="txt_nama_terminal" placeholder="Ex: Tawang Alun" />
                                        </div>
                                        <div class="col-12 mb-3">
                                            <label for="InputAlamat" class="form-label">Alamat Terminal</label>
                                            <textarea class="form-control form-control-user2" id="InputAlamat" name="txt_detail_alamat_terminal"
                                                placeholder="Ex: Jl. Dharmawangsa"></textarea>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <label for="InputProvTerminal" class="form-label">Provinsi</label>
                                            <input type="text" class="form-control form-control-user2" id="InputProvTerminal"
                                                name="d_provinsi_terminal" placeholder="Ex: Jawa Timur" />
                                        </div>
                                        <div class="col-6 mb-3">
                                            <label for="InputKabupatenTerminal" class="form-label">Kabupaten</label>
                                            <input type="text" class="form-control form-control-user2" id="InputKabupatenTerminal"
                                                name="d_kabupaten_terminal" placeholder="Ex: Jember" />
                                        </div>
                                        <div class="col-6 mb-3">
                                            <label for="InputKecamatanTerminal" class="form-label">Kecamatan</label>
                                            <input type="text" class="form-control form-control-user2" id="InputKecamatanTerminal"
                                                name="d_kecamatan_terminal" placeholder="Ex: Rambupuji" />
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="button" class="btn btn-secondary roundedBtn" data-bs-dismiss="modal" value="Cancel" />
                                        <input type="submit" name="simpan" class="btn colorPrimary text-white roundedBtn" value="Simpan" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <script src="plugin/js/bootstrap.bundle.min.js"></script>
                <script src="jquery/jquery-3.6.0.min.js"></script>
                <script src="plugin/js/form-validation.init.js"></script>
                <script src="plugin/js/parsley.min.js"></script>
                <script src="plugin/js/javascript.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.3/dist/sweetalert2.all.min.js" integrity="sha256-+InBGKGbhOQiyCbWrARmIEICqZ8UvYJr/qVhHmlmFpc=" crossorigin="anonymous"></script>
                <script src="plugin/js/custom_SweetAlert2.js"></script>
            </body>
        )
    }
}
export default Register;