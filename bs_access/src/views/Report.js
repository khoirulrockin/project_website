import React, { Component } from "react";
import { Link } from 'react-router-dom';
import '../assets/css/style.css';
import axios from 'axios';

import register from "../assets/images/register.png";
import bs_access from "../assets/images/bs-access.png";
// import login from "../assets/images/register.png";
// import logo from "../assets/images/logoW.png";
// import bus3 from "../assets/images/bus3.png";
// import bus2 from "../assets/images/bus2.png";
import data_studio_icon2 from '../assets/images/ico/icoBus_noFill.png';
import data_admin_icon2 from '../assets/images/ico/icoDriver_noFill.png';
import pemesanan_icon2 from '../assets/images/ico/icoBooking_no Fill.png';
import laporan_icon2 from '../assets/images/ico/icoReport_noFill.png';
import dashboard_icon from '../assets/images/ico/icoDash_noFill.png';
import sumber_data_icon from '../assets/images/ico/icoData_noFill.png';
import data_studio_icon from '../assets/images/ico/icoBus_noFill.png';
import data_admin_icon from '../assets/images/ico/icoDriver_noFill.png';
import data_user_icon from '../assets/images/ico/iconProfile_noFill.png';
import pemesanan_icon from '../assets/images/ico/icoBooking_no Fill.png';
import laporan_icon from '../assets/images/ico/icoReport_noFill.png';


class Report extends Component{
    render() {
        return (
            <body>
                <div className="sidebar">
                    <div className="logo-details">
                        <i className="fas fa-bus">
                            <span className="logo_name">BS<span className="logo_nameMin"> Access</span></span>
                        </i>
                    </div>
                    <ul className="nav-links">
                        <li className="sidebar-heading mb-2 p-0">Menu :</li>
                        <li className="nav-item ">
                            <a href="/dashboard" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico" src={dashboard_icon} alt="logo1" data-bs-toggle="collapse"
                                        data-bs-target="#dashboard" aria-expanded="false" aria-controls="dashboard" />
                                </div>
                                <span className="link_name">Dashboard</span>
                                <i className="bx bxs-chevron-right arrow" data-bs-toggle="collapse" data-bs-target="#dashboard"
                                    aria-expanded="false" aria-controls="dashboard"></i>
                            </a>
                            <div id="dashboard" className="collapse">
                                <ul className="sub-menu">
                                    <li><a className="link_name" href="/dashboard">Dashboard</a></li>
                                    <li><a href="#">Grafik</a></li>
                                    <li><a href="#">Log</a></li>
                                    <li><a href="#">Pengaturan</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                        </li>
                        <li className="sidebar-heading mt-2 p-0">List Data</li>
                        <li className="nav-item ">
                            <a href="/sumber-data" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={sumber_data_icon} alt="logo2" data-bs-toggle="collapse"
                                        data-bs-target="#SumberData" aria-expanded="false" aria-controls="SumberData" />
                                </div>
                                <span className="link_name">Source</span>
                                <i className="bx bxs-chevron-right arrow" data-bs-toggle="collapse" data-bs-target="#SumberData"
                                    aria-expanded="false" aria-controls="SumberData"></i>
                            </a>
                            <div id="SumberData" className="collapse">
                                <ul className="sub-menu">
                                    <li><a className="link_name" href="sumberData.php">Sumber Data</a></li>
                                    <li><a href="#">Terminal</a></li>
                                    <li><a href="#">Jenis Bus</a></li>
                                    <li><a href="#">Rute User</a></li>
                                    <li><a href="#">Penumpang</a></li>
                                    <li><a href="#">Staff</a></li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-item ">
                            <a href="data-studio" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_studio_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data Studio</span>
                            </a>
                        </li>
                        <li className="nav-item ">
                            <a href="data-admin" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_admin_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data Admin</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataDriver.php">Data Driver</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a href="data-user" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={data_user_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Data User</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataAkun.php">Data Akun</a></li>
                            </ul>
                        </li>
                        <li>
                        </li>
                        <li className="sidebar-heading mt-2 p-0">Layanan</li>
                        <li className="nav-item">
                            <a href="data-pemesanan" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={pemesanan_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Booking</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataPemesanan.php">Booking</a></li>
                            </ul>
                        </li>
                        <li className="nav-item  active mb-1">
                            <a href="data-laporan" className="focusMenu">
                                <div className="frame-ico">
                                    <img className="ico2" src={laporan_icon} alt="logo1" />
                                </div>
                                <span className="link_name">Report</span>
                            </a>
                            <ul className="sub-menu blank">
                                <li><a className="link_name" href="dataLaporan.php">Report</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <div className="profile-details">
                                <div className="profile-content">
                                    <img src="https://upload.wikimedia.org/wikipedia/id/e/ea/Prince_zuko.jpg" />
                                </div>
                                <div className="name-job">
                                    <div className="profile_name">
                                    </div>
                                    <div className="job">Admin</div>
                                </div>
                                <a className="" href="logout.php"> <i className="bx bx-log-out"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
                {/* <div className="post-studio"> */}
                    {/* CONTENT */}
                    <div className="home-section">
                        <div className="home-content d-flex justify-content-end align-items-center mb-4">
                            <div className="menu">
                                <i className="fas fa-bars"></i>
                            </div>
                            <nav className="custNav">
                                <ul className="nav">
                                    <li className="nav-item">
                                        <a href="#" className="nav-link transition">
                                            <i className="far fa-bell"></i>
                                            <span className="badge alert-danger p-1"> Staff</span>
                                        </a>
                                    </li>

                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" id="dropdownProfile" role="button" data-bs-toggle="dropdown"
                                            aria-expanded="false">
                                            <span className="RobotoReg14"></span>
                                            <img className="img-profile rounded-circle" src="https://upload.wikimedia.org/wikipedia/id/e/ea/Prince_zuko.jpg" />
                                        </a>

                                        <ul className="dropdown-menu border-0 dropdown-menu-end shadow" aria-labelledby="dropdownProfile">
                                            <li>
                                                <a className="dropdown-item" data-bs-toggle="modal"
                                                    data-bs-target="#editDataAdministrator<?php echo $sesID ?>"><i className="las la-user mr-2"></i>My
                                                    Profile</a>
                                            </li>
                                            <li>
                                                <a className="dropdown-item" href="#"> <i className="las la-list-alt mr-2"></i> Activity Log </a>
                                            </li>
                                            <li>
                                                <div className="dropdown-divider"></div>
                                            </li>
                                            <li>
                                                <a className="dropdown-item" href="logout.php"> <i className="las la-sign-out-alt mr-2"></i> Sign Out </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div className="row m-0 px-3 rowCustom">
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientBlue shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Studio</div>
                                                <div className="RobotoBold18 text-white">
                                                    <span> Studio</span></div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={data_studio_icon2} alt="logoBus" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientPink shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Admin</div>
                                                <div className="RobotoBold18 text-white">(Belum)</div>
                                            </div>
                                            <div className="col-auto">
                                            </div>
                                            {/* <img src={data_admin_icon2} className="icon-data" alt="ver" /> */}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientYellow shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Data Pemesanan</div>
                                                <div className="RobotoBold18 text-white">
                                                    Pe</div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={pemesanan_icon2} alt="et" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 gradientGreen shadow h-100 py-2 rounded">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="RobotoReg14 text-white">Total Penghasilan</div>
                                                <div className="RobotoBold18 text-white"><span>Rp.</span>
                                                    hkfdkhk</div>
                                            </div>
                                            <div className="col-auto">
                                                <img src={laporan_icon2} alt="y" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            {/* Panel */}
                            <div class="row g-2 m-0 px-4">
                            <div class="col-12">
                                <div class="colorSecondary2 shadow roundedPanel">
                                
                                <ul class="nav nav-tabs bg-white pt-2 px-4 roundedTab custShadow" id="ex1" role="tablist">
                                    <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tabs-1" type="button"
                                        role="tab" aria-controls="tabs-1" aria-selected="true">Terminal</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tab-2" data-bs-toggle="tab" data-bs-target="#tabs-2" type="button" role="tab"
                                        aria-controls="tabs-2" aria-selected="false">Jenis Bus</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tab-3" data-bs-toggle="tab" data-bs-target="#tabs-3" type="button" role="tab"
                                        aria-controls="tabs-3" aria-selected="false">Rute</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tab-4" data-bs-toggle="tab" data-bs-target="#tabs-4" type="button" role="tab"
                                        aria-controls="tabs-4" aria-selected="false">Penumpang</button>
                                    </li>
                                </ul>
                                {/* <!-- Tab terminal --> */}
                                <div class="tab-content mb-5" id="ex1-content">
                                    <div class="tab-pane show active" id="tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1">
                                    <div class="row g-2 m-0">
                                        <div class="col-lg-12 p-0 m-0">
                                        <div class="card mb-4 roundedTabContent">
                                            <div class="card-header shadow roundedTabContent">
                                            <div class="title float-start">
                                                <span class="m-0"><b>Tabel Data Terminal</b></span>
                                            </div>
                                            <div class="btnAction float-end">
                                                <button class="btn btn-light text-dark btn-circle custShadow2 me-2" data-bs-toggle="modal"
                                                data-bs-target="#tambahDataTerminal"><i class="fas fa-plus" data-bs-toggle="tooltip"
                                                    title="Tambah Data"></i></button>
                                                {/* <!-- <button class="btn btn-light text-danger btn-circle custShadow2" data-bs-toggle="modal" data-bs-target="#deleteDataTerminal"><i class="fas fa-trash" data-bs-toggle="tooltip" title="Hapus Data"></i></button> --> */}
                                            </div>
                                            </div>
                                            <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover dataTable nowrap" width="100%">
                                                <thead>
                                                    <tr>
                                                    <th class="cb">
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" class="selectAll" />
                                                        <label for="selectAll"></label>
                                                        </span>
                                                    </th>
                                                    <th class="actions">Action</th>
                                                    <th class="id">ID</th>
                                                    <th class="terminal">Terminal</th>
                                                    <th class="alamat">Alamat</th>
                                                    <th class="provinsis">Provinsi</th>
                                                    <th class="kabupatens">Kabupaten/Kota</th>
                                                    <th class="kecamatans">Kecamatan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    {/* <tr> */}
                                                    <td>
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" id="checkbox1" name="option[]" value="<?php echo $no; ?>" />
                                                        <label for="checkbox1"></label>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="actionBtn" aria-label="Edit">
                                                        <button class="btn btn-success btn-user btn-circle" aria-label="EditModal"
                                                            data-bs-toggle="modal" data-bs-target="#editDataTerminal<?php echo $id_terminal ?>"
                                                            value="edit">
                                                            &nbsp;<i class="fa fa-edit fa-sm" data-bs-toggle="tooltip" title="Edit"></i>
                                                        </button>
                                                        </a>
                                                        <a href="#" class="actionBtn" aria-label="Delete">
                                                        <button class="btn btn-danger btn-user btn-circle" aria-label="DeleteModal"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#deleteDataTerminal<?php echo $id_terminal ?>" value="hapus">
                                                            <i class="fa fa-trash fa-sm" data-bs-toggle="tooltip" title="Delete"></i>
                                                        </button>
                                                        </a>

                                                        {/* <!-- Edit Modal --> */}
                                                        <div id="editDataTerminal<?php echo $id_terminal ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content modal-edit">
                                                            <form role="form" action="editTerminal.php" method="POST">
                                                                
                                                                <div class="modal-header">
                                                                <h4 class="modal-title">Edit Data Terminal</h4>
                                                                <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                                    data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                                    <i class="fa fa-times fa-sm"></i>
                                                                </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12 mb-3" hidden>
                                                                    <label for="inputId" class="form-label">Id</label>
                                                                    <input type="text" class="form-control form-control-user2" id="inputId"
                                                                        name="txt_id_terminal" value="<?php echo $id_terminal ?>" placeholder=""
                                                                        readonly />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputTerminal" class="form-label">Nama Terminal</label>
                                                                    <input type="text" class="form-control form-control-user2"
                                                                        id="inputTerminal" name="txt_nama_terminal" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        placeholder="Ex: Tawang Alun" value="<?php echo $terminal ?>" />
                                                                    </div>
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputAlamat" class="form-label">Alamat Terminal</label>
                                                                    <input type="text" class="form-control form-control-user2"
                                                                        id="inputAlamat" name="txt_detail_alamat_terminal"
                                                                        placeholder="Ex: Jl. Dharmawangsa" value="<?php echo $alamat ?>"
                                                                        required data-parsley-required-message="Data harus di isi !!!" />
                                                                    </div>
                                                                    <div class="col-12 mb-3">
                                                                    <label for="InputProvTerminal" class="form-label">Provinsi</label>
                                                                    <input type="text" class="form-control form-control-user2"
                                                                        id="InputProvTerminal" name="d_provinsi_terminal"
                                                                        placeholder="Ex: Jawa Timur" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        value="<?php echo $provinsi ?>" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-6 mb-3">
                                                                    <label for="InputKabupatenTerminal" class="form-label">Kabupaten</label>
                                                                    <input type="text" class="form-control form-control-user2"
                                                                        id="InputKabupatenTerminal" name="d_kabupaten_terminal"
                                                                        placeholder="Ex: Jember" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        value="<?php echo $kabupaten ?>" />
                                                                    </div>
                                                                    <div class="col-6 mb-3">
                                                                    <label for="InputKecamatanTerminal" class="form-label">Kecamatan</label>
                                                                    <input type="text" class="form-control form-control-user2"
                                                                        id="InputKecamatanTerminal" name="d_kecamatan_terminal"
                                                                        placeholder="Ex: Rambupuji" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        value="<?php echo $kecamatan ?>" />
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button class="btn btn-secondary roundedBtn" type="button"
                                                                    data-bs-dismiss="modal">Batal</button>
                                                                    <button type="submit" class="btn text-white colorPrimary roundedBtn"
                                                                    name="simpan">Update</button>
                                                                </div>
                                                                </div>
                                                            </form>
                                                            
                                                            </div>
                                                        </div>
                                                        </div>

                                                        {/* <!-- Delete Modal --> */}
                                                        <div id="deleteDataTerminal<?php echo $id_terminal; ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                            <form action="">
                                                                <div class="modal-header">
                                                                <h4 class="modal-title">Hapus Terminal</h4>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                    aria-label="Close" aria-hidden="true"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                <p>Apakah Anda yakin ingin menghapus data terminal ini ?</p>
                                                                <p class="text-warning"><small>Perlu hati-hati karena data akan hilang
                                                                    selamanya !</small></p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                <button class="btn btn-secondary" type="button"
                                                                    data-bs-dismiss="modal">Batal</button>
                                                                <a class="btn btn-danger"
                                                                    href="hapusTerminal.php?id_terminal=<?php echo $id_terminal; ?>">Hapus</a>
                                                                </div>
                                                            </form>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </td>
                                                    {/* <td>TRML000<?php echo $id_terminal; ?></td>
                                                    <td><?php echo $terminal; ?></td>
                                                    <td><?php echo $alamat; ?></td>
                                                    <td><?php echo $provinsi; ?></td>
                                                    <td><?php echo $kabupaten; ?></td>
                                                    <td><?php echo $kecamatan; ?></td>
                                                    </tr>
                                                    <?php
                                                        $no++;
                                                    }
                                                    }
                                                    ?> */}
                                                </tbody>
                                                </table>
                                            </div>
                                            </div>

                                            {/* <!-- Tambah Modal --> */}
                                            <div id="tambahDataTerminal" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content modal-edit">
                                                <form role="form" action="tambahTerminal.php" method="POST">
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Data Terminal</h4>
                                                    <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                        data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                        <i class="fa fa-times fa-sm"></i>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputTerminal" class="form-label">Nama Terminal</label>
                                                        <input type="text" class="form-control form-control-user2" id="inputTerminal"
                                                            name="txt_nama_terminal" required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Tawang Alun" />
                                                        </div>
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputAlamat" class="form-label">Alamat Terminal</label>
                                                        <input type="text" class="form-control form-control-user2" id="inputAlamat"
                                                            name="txt_detail_alamat_terminal" required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Jl. Dharmawangsa" />
                                                        </div>
                                                        <div class="col-12 mb-3">
                                                        <label for="InputProvTerminal" class="form-label">Provinsi</label>
                                                        <input type="text" class="form-control form-control-user2" id="InputProvTerminal"
                                                            name="d_provinsi_terminal" required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: Jawa Timur" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-6 mb-3">
                                                        <label for="InputKabupatenTerminal" class="form-label">Kabupaten</label>
                                                        <input type="text" class="form-control form-control-user2" id="InputKabupatenTerminal"
                                                            name="d_kabupaten_terminal" required
                                                            data-parsley-required-message="Data harus di isi !!!" placeholder="Ex: Jember" />
                                                        </div>
                                                        <div class="col-6 mb-3">
                                                        <label for="InputKecamatanTerminal" class="form-label">Kecamatan</label>
                                                        <input type="text" class="form-control form-control-user2" id="InputKecamatanTerminal"
                                                            name="d_kecamatan_terminal" required
                                                            data-parsley-required-message="Data harus di isi !!!" placeholder="Ex: Rambupuji" />
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <input type="button" class="btn btn-secondary roundedBtn" data-bs-dismiss="modal"
                                                        value="Cancel" />
                                                        <input type="submit" name="simpan" class="btn colorPrimary text-white roundedBtn"
                                                        value="Simpan" />
                                                    </div>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>

                                    {/* <!-- Tab Jenis Bus --> */}
                                    <div class="tab-pane" id="tabs-2" role="tabpanel" aria-labelledby="tab-2">
                                    <div class="row g-2 m-0">
                                        <div class="col-lg-12 p-0 m-0">
                                        <div class="card mb-4 roundedTabContent">
                                            <div class="card-header shadow roundedTabContent">
                                            <div class="title float-start">
                                                <span class="m-0"><b>Tabel Data Jenis Bus</b></span>
                                            </div>
                                            <div class="btnAction float-end">
                                                <button class="btn btn-light text-dark btn-circle custShadow2 me-2" data-bs-toggle="modal"
                                                data-bs-target="#tambahDataJenisBus"><i class="fas fa-plus" data-bs-toggle="tooltip"
                                                    title="Tambah Data"></i></button>
                                                {/* <!-- <button class="btn btn-light text-danger btn-circle custShadow2" data-bs-toggle="modal" data-bs-target="#deleteDataJenisBus"><i class="fas fa-trash" data-bs-toggle="tooltip" title="Hapus Data"></i></button> --> */}
                                            </div>
                                            </div>
                                            <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover dataTable nowrap" width="100%" id="tabelJenisBus">
                                                <thead>
                                                    <tr>
                                                    <th class="cb">
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" class="selectAll" />
                                                        <label for="selectAll"></label>
                                                        </span>
                                                    </th>
                                                    <th class="actions">Action</th>
                                                    <th class="id">ID Jenis Bus</th>
                                                    <th class="jenis">Jenis Bus</th>
                                                    <th class="fasilitas">Fasilitas</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {/* <?php
                                                    $data = $obj->lihatJenisBus();
                                                    $no = 1;
                                                    if ($data->rowCount() > 0) {
                                                    if ($sesLvl == 1) {
                                                        $dis = "";
                                                    } else {
                                                        $dis = "disabled";
                                                    }
                                                    while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                                                        $id_jenis = $row['id_jenis'];
                                                        $jenis = $row['jenis'];
                                                        $fasilitas = $row['fasilitas'];
                                                    ?> */}
                                                    {/* <tr> */}
                                                    <td>
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" id="checkbox1" name="option[]" value="<?php echo $no; ?>" />
                                                        <label for="checkbox1"></label>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="actionBtn" aria-label="Edit">
                                                        <button class="btn btn-success btn-user btn-circle" aria-label="EditModal"
                                                            data-bs-toggle="modal" data-bs-target="#editDataJenisBus<?php echo $id_jenis ?>"
                                                            value="edit">
                                                            &nbsp;<i class="fa fa-edit fa-sm" data-bs-toggle="tooltip" title="Edit"></i>
                                                        </button>
                                                        </a>
                                                        <a href="#" class="actionBtn" aria-label="Delete">
                                                        <button class="btn btn-danger btn-user btn-circle" aria-label="DeleteModal"
                                                            data-bs-toggle="modal" data-bs-target="#deleteDataJenisBus<?php echo $id_jenis ?>"
                                                            value="hapus">
                                                            <i class="fa fa-trash fa-sm" data-bs-toggle="tooltip" title="Delete"></i>
                                                        </button>
                                                        </a>

                                                        {/* <!-- Edit Modal --> */}
                                                        <div id="editDataJenisBus<?php echo $id_jenis ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content modal-edit">
                                                            <form role="form" action="editJenisBus.php" method="POST">
                                                                {/* <?php
                                                                    $query = $obj->pilihJenisBus($id_jenis);
                                                                    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                                                                    $id_jenis2 = $row['id_jenis'];
                                                                    $jenis2 = $row['jenis'];
                                                                    $fasilitas2 = $row['fasilitas'];
                                                                    ?> */}
                                                                <div class="modal-header">
                                                                <h4 class="modal-title">Edit Data Jenis Bus</h4>
                                                                <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                                    data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                                    <i class="fa fa-times fa-sm"></i>
                                                                </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12 mb-3" hidden>
                                                                    <label for="inputId" class="form-label">Id</label>
                                                                    <input type="text" class="form-control form-control-user2" id="inputId"
                                                                        name="txt_id_jenis" value="<?php echo $id_jenis2; ?>" placeholder=""
                                                                        readonly />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputJenis" class="form-label">Jenis</label>
                                                                    <input type="text" class="form-control form-control-user2" id="inputJenis"
                                                                        name="txt_jenis" placeholder="Ex: AKAS" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        value="<?php echo $jenis2; ?>" />
                                                                    </div>
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputFasilitas" class="form-label">Fasilitas</label>
                                                                    <input type="text" class="form-control form-control-user2"
                                                                        id="inputFasilitas" name="txt_fasilitas"
                                                                        placeholder="Ex: TV, AC, Wifi, Toilet" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        value="<?php echo $fasilitas2; ?>" />
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button class="btn btn-secondary roundedBtn" type="button"
                                                                    data-bs-dismiss="modal">Batal</button>
                                                                    <button type="submit" class="btn text-white colorPrimary roundedBtn"
                                                                    name="simpan">Update</button>
                                                                </div>
                                                                </div>
                                                            </form>
                                                            {/* <?php
                                                                    }
                                                                ?> */}
                                                            </div>
                                                        </div>
                                                        </div>

                                                        {/* <!-- Delete Modal --> */}
                                                        <div id="deleteDataJenisBus<?php echo $id_jenis; ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                            <form action="">
                                                                <div class="modal-header">
                                                                <h4 class="modal-title">Hapus Jenis Bus</h4>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                    aria-label="Close" aria-hidden="true"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                <p>Apakah Anda yakin ingin menghapus data jenis bus ini ?</p>
                                                                <p class="text-warning"><small>Perlu hati-hati karena data akan hilang
                                                                    selamanya !</small></p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                <button class="btn btn-secondary" type="button"
                                                                    data-bs-dismiss="modal">Batal</button>
                                                                <a class="btn btn-danger"
                                                                    href="hapusJenisBus.php?id_jenis=<?php echo $id_jenis; ?>">Hapus</a>
                                                                </div>
                                                            </form>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </td>
                                                    {/* <td>JB000<?php echo $id_jenis; ?></td>
                                                    <td><?php echo $jenis; ?></td>
                                                    <td><?php echo $fasilitas; ?></td>
                                                    </tr>
                                                    <?php
                                                        $no++;
                                                    }
                                                    }
                                                    ?> */}
                                                </tbody>
                                                </table>
                                            </div>
                                            </div>

                                            {/* <!-- Tambah Modal --> */}
                                            <div id="tambahDataJenisBus" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content modal-edit">
                                                <form role="form" action="tambahJenisBus.php" method="POST">
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Data Jenis Bus</h4>
                                                    <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                        data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                        <i class="fa fa-times fa-sm"></i>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputJenis" class="form-label">Jenis</label>
                                                        <input type="text" class="form-control form-control-user2" id="inputJenis"
                                                            name="txt_jenis" placeholder="Ex: AKAS" required
                                                            data-parsley-required-message="Data harus di isi !!!" />
                                                        </div>
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputFasilitas" class="form-label">Fasilitas</label>
                                                        <input type="text" class="form-control form-control-user2" id="inputFasilitas"
                                                            name="txt_fasilitas" placeholder="Ex: TV, AC, Wifi, Toilet" required
                                                            data-parsley-required-message="Data harus di isi !!!" />
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="button" class="btn btn-secondary roundedBtn" data-bs-dismiss="modal"
                                                        value="Cancel" />
                                                        <input type="submit" name="simpan" class="btn colorPrimary text-white roundedBtn"
                                                        value="Simpan" />
                                                    </div>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>

                                    {/* <!-- Tabs Rute --> */}
                                    <div class="tab-pane" id="tabs-3" role="tabpanel" aria-labelledby="ex1-tab-3">
                                    <div class="row g-2 m-0">
                                        <div class="col-lg-12 p-0 m-0">
                                        <div class="card mb-4 roundedTabContent">
                                            <div class="card-header shadow roundedTabContent">
                                            <div class="title float-start">
                                                <span class="m-0"><b>Tabel Data Rute</b></span>
                                            </div>
                                            <div class="btnAction float-end">
                                                <button class="btn btn-light text-dark btn-circle custShadow2 me-2" data-bs-toggle="modal"
                                                data-bs-target="#tambahDataRute"><i class="fas fa-plus" data-bs-toggle="tooltip"
                                                    title="Tambah Data"></i></button>
                                                {/* <!-- <button class="btn btn-light text-danger btn-circle custShadow2" data-bs-toggle="modal" data-bs-target="#deleteDataRute"><i class="fas fa-trash" data-bs-toggle="tooltip" title="Hapus Data"></i></button> --> */}
                                            </div>
                                            </div>
                                            <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover dataTable nowrap" width="100%">
                                                <thead>
                                                    <tr>
                                                    <th class="cb">
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" class="selectAll" />
                                                        <label for="selectAll"></label>
                                                        </span>
                                                    </th>
                                                    <th class="actions">Action</th>
                                                    <th class="id">ID</th>
                                                    <th class="pemberangkatan">Pemberangkatan</th>
                                                    <th class="waktu_berangkat">Waktu Berangkat</th>
                                                    <th class="tujuan">Tujuan</th>
                                                    <th class="waktu_tiba">Waktu Tiba</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {/* <?php
                                                    $data = $obj->lihatRute();
                                                    $no = 1;
                                                    if ($data->rowCount() > 0) {
                                                    if ($sesLvl == 1) {
                                                        $dis = "";
                                                    } else {
                                                        $dis = "disabled";
                                                    }
                                                    while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                                                        $id_rute = $row['id_rute'];
                                                        $pemberangkatan = $row['pemberangkatan'];
                                                        $waktu_berangkat = $row['waktu_berangkat'];
                                                        $tujuan = $row['tujuan'];
                                                        $waktu_tiba = $row['waktu_tiba'];
                                                    ?> */}

                                                    {/* <tr> */}
                                                    <td>
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" id="checkbox1" name="option[]" value="<?php echo $no; ?>" />
                                                        <label for="checkbox1"></label>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="actionBtn" aria-label="Edit">
                                                        <button class="btn btn-success btn-user btn-circle" aria-label="EditModal"
                                                            data-bs-toggle="modal" data-bs-target="#editDataRute<?php echo $id_rute ?>"
                                                            value="edit">
                                                            &nbsp;<i class="fa fa-edit fa-sm" data-bs-toggle="tooltip" title="Edit"></i>
                                                        </button>
                                                        </a>
                                                        <a href="#" class="actionBtn" aria-label="Delete">
                                                        <button class="btn btn-danger btn-user btn-circle" aria-label="DeleteModal"
                                                            data-bs-toggle="modal" data-bs-target="#deleteDataRute<?php echo $id_rute ?>"
                                                            value="hapus">
                                                            <i class="fa fa-trash fa-sm" data-bs-toggle="tooltip" title="Delete"></i>
                                                        </button>
                                                        </a>

                                                        {/* <!-- Edit Modal --> */}
                                                        <div id="editDataRute<?php echo $id_rute ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content modal-edit">
                                                            <form role="form" action="editRute.php" method="POST">
                                                                {/* <?php
                                                                    $query = $obj->pilihRute($id_rute);
                                                                    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                                                                    $id_rute2 = $row['id_rute'];
                                                                    $pemberangkatan2 = $row['pemberangkatan'];
                                                                    $waktu_berangkat2 = $row['waktu_berangkat'];
                                                                    $tujuan2 = $row['tujuan'];
                                                                    $waktu_tiba2 = $row['waktu_tiba'];
                                                                    ?> */}
                                                                <div class="modal-header">
                                                                <h4 class="modal-title">Edit Data Rute</h4>
                                                                <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                                    data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                                    <i class="fa fa-times fa-sm"></i>
                                                                </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12 mb-3" hidden>
                                                                    <label for="inputId" class="form-label">Id</label>
                                                                    <input type="text" class="form-control form-control-user2" id="inputId"
                                                                        name="txt_id_rute" value="<?php echo $id_rute2 ?>" placeholder=""
                                                                        readonly />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="InputIdTerminal" class="form-label">Pemberangkatan</label>
                                                                    <select class="form-select form-select-user select-md"
                                                                        aria-label=".form-select-sm example" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        name="txt_pemberangkatan">
                                                                        <option disabled selected>Pilih Terminal</option>
                                                                        {/* <?php
                                                                            $datas = $obj->lihatTerminal();
                                                                            $no = 1;
                                                                            if ($datas->rowCount() > 0) {
                                                                                if ($sesLvl == 1) {
                                                                                $dis = "";
                                                                                } else {
                                                                                $dis = "disabled";
                                                                                }
                                                                                while ($row = $datas->fetch(PDO::FETCH_ASSOC)) {
                                                                                $id_terminal = $row['id_terminal'];
                                                                                $nama_terminal = $row['nama_terminal'];
                                                                                $kabupaten = $row['kabupaten_terminal'];
                                                                            ?>
                                                                        <option value="<?php echo $id_terminal; ?>">
                                                                        <?php echo $nama_terminal, ', ', $kabupaten; ?></option>
                                                                        <?php
                                                                                }
                                                                            }
                                                                            ?> */}
                                                                    </select>
                                                                    </div>
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputWaktuBerangkat" class="form-label">Waktu
                                                                        Berangkat</label>
                                                                    <input type="time" class="form-control form-control-user2"
                                                                        id="inputWaktuBerangkat" name="txt_waktu_berangkat" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        placeholder="Ex: 00.00" value="<?php echo $waktu_berangkat2 ?>"></input>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="InputIdTerminal" class="form-label">Tujuan</label>
                                                                    <select class="form-select form-select-user select-md"
                                                                        aria-label=".form-select-sm example" required
                                                                        data-parsley-required-message="Data harus di isi !!!" name="txt_tujuan">
                                                                        <option disabled selected>Pilih Terminal</option>
                                                                        {/* <?php
                                                                            $datasd = $obj->lihatTerminal();
                                                                            $no = 1;
                                                                            if ($datasd->rowCount() > 0) {
                                                                                if ($sesLvl == 1) {
                                                                                $dis = "";
                                                                                } else {
                                                                                $dis = "disabled";
                                                                                }
                                                                                while ($row = $datasd->fetch(PDO::FETCH_ASSOC)) {
                                                                                $id_terminals = $row['id_terminal'];
                                                                                $nama_terminals = $row['nama_terminal'];
                                                                                $kabupatens = $row['kabupaten_terminal'];
                                                                            ?>
                                                                        <option value="<?php echo $id_terminals; ?>">
                                                                        <?php echo $nama_terminals, ', ', $kabupatens; ?></option>
                                                                        <?php
                                                                                }
                                                                            }
                                                                            ?> */}
                                                                    </select>
                                                                    </div>
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputWaktuTiba" class="form-label">Waktu Tiba</label>
                                                                    <input type="time" class="form-control form-control-user2"
                                                                        id="inputWaktuTiba" name="txt_waktu_tiba" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        placeholder="Ex: 06.00" value="<?php echo $waktu_tiba2 ?>"></input>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button class="btn btn-secondary roundedBtn" type="button"
                                                                    data-bs-dismiss="modal">Batal</button>
                                                                    <button type="submit" class="btn text-white colorPrimary roundedBtn"
                                                                    name="simpan">Update</button>
                                                                </div>
                                                                </div>
                                                            </form>
                                                            {/* <?php
                                                                    }
                                                                ?> */}
                                                            </div>
                                                        </div>
                                                        </div>

                                                        {/* <!-- Delete Modal --> */}
                                                        <div id="deleteDataRute<?php echo $id_rute; ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                            <form action="">
                                                                <div class="modal-header">
                                                                <h4 class="modal-title">Hapus Rute</h4>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                    aria-label="Close" aria-hidden="true"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                <p>Apakah Anda yakin ingin menghapus data rute ini ?</p>
                                                                <p class="text-warning"><small>Perlu hati-hati karena data akan hilang
                                                                    selamanya !</small></p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                <button class="btn btn-secondary" type="button"
                                                                    data-bs-dismiss="modal">Batal</button>
                                                                <a class="btn btn-danger"
                                                                    href="hapusRute.php?id_rute=<?php echo $id_rute; ?>">Hapus</a>
                                                                </div>
                                                            </form>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </td>
                                                    {/* <td>RT000<?php echo $id_rute; ?></td>
                                                    <td><?php echo $pemberangkatan; ?></td>
                                                    <td><?php echo $waktu_berangkat; ?> WIB</td>
                                                    <td><?php echo $tujuan; ?></td>
                                                    <td><?php echo $waktu_tiba; ?> WIB</td>
                                                    </tr>
                                                    <?php
                                                        $no++;
                                                    }
                                                    }
                                                    ?> */}
                                                </tbody>
                                                </table>
                                            </div>
                                            </div>

                                            {/* <!-- Tambah Modal --> */}
                                            <div id="tambahDataRute" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content modal-edit">
                                                <form role="form" action="tambahRute.php" method="POST">
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Data Rute</h4>
                                                    <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                        data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                        <i class="fa fa-times fa-sm"></i>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="InputIdTerminal" class="form-label">Pemberangkatan</label>
                                                        <select class="form-select form-select-user select-md"
                                                            aria-label=".form-select-sm example" required
                                                            data-parsley-required-message="Data harus di isi !!!" name="txt_pemberangkatan">
                                                            <option disabled selected>Pilih Terminal</option>
                                                            {/* <?php
                                                            $datas = $obj->lihatTerminal();
                                                            $no = 1;
                                                            if ($datas->rowCount() > 0) {
                                                            if ($sesLvl == 1) {
                                                                $dis = "";
                                                            } else {
                                                                $dis = "disabled";
                                                            }
                                                            while ($row = $datas->fetch(PDO::FETCH_ASSOC)) {
                                                                $id_terminalt = $row['id_terminal'];
                                                                $nama_terminalt = $row['nama_terminal'];
                                                                $kabupatent = $row['kabupaten_terminal'];
                                                            ?>
                                                            <option value="<?php echo $id_terminalt; ?>">
                                                            <?php echo $nama_terminalt, ', ', $kabupatent; ?></option>
                                                            <?php
                                                            }
                                                            }
                                                            ?> */}
                                                        </select>
                                                        </div>
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputWaktuBerangkat" class="form-label">Waktu Berangkat</label>
                                                        <input type="time" class="form-control form-control-user2" id="inputWaktuBerangkat"
                                                            name="txt_waktu_berangkat" required
                                                            data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: 00.00"></input>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="InputIdTerminal" class="form-label">Tujuan</label>
                                                        <select class="form-select form-select-user select-md"
                                                            aria-label=".form-select-sm example" required
                                                            data-parsley-required-message="Data harus di isi !!!" name="txt_tujuan">
                                                            <option disabled selected>Pilih Terminal</option>
                                                            {/* <?php
                                                            $datasd = $obj->lihatTerminal();
                                                            $no = 1;
                                                            if ($datasd->rowCount() > 0) {
                                                            if ($sesLvl == 1) {
                                                                $dis = "";
                                                            } else {
                                                                $dis = "disabled";
                                                            }
                                                            while ($row = $datasd->fetch(PDO::FETCH_ASSOC)) {
                                                                $id_terminalst = $row['id_terminal'];
                                                                $nama_terminalst = $row['nama_terminal'];
                                                                $kabupatenst = $row['kabupaten_terminal'];
                                                            ?>
                                                            <option value="<?php echo $id_terminalst; ?>">
                                                            <?php echo $nama_terminalst, ', ', $kabupatenst; ?></option>
                                                            <?php
                                                            }
                                                            }
                                                            ?> */}
                                                        </select>
                                                        </div>
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputWaktuTiba" class="form-label">Waktu Tiba</label>
                                                        <input type="time" class="form-control form-control-user2" id="inputWaktuTiba"
                                                            name="txt_waktu_tiba" required data-parsley-required-message="Data harus di isi !!!"
                                                            placeholder="Ex: 06.00"></input>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <input type="button" class="btn btn-secondary roundedBtn" data-bs-dismiss="modal"
                                                        value="Cancel" />
                                                        <input type="submit" name="simpan" class="btn colorPrimary text-white roundedBtn"
                                                        value="Simpan" />
                                                    </div>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>

                                    {/* <!-- Tabs Penumpang   --> */}
                                    <div class="tab-pane" id="tabs-4" role="tabpanel" aria-labelledby="ex1-tab-4">
                                    <div class="row g-2 m-0">
                                        <div class="col-lg-12 p-0 m-0">
                                        <div class="card mb-4 roundedTabContent">
                                            <div class="card-header shadow roundedTabContent">
                                            <div class="title float-start">
                                                <span class="m-0"><b>Tabel Data Penumpang</b></span>
                                            </div>
                                            <div class="btnAction float-end">
                                                <button class="btn btn-light text-dark btn-circle custShadow2 me-2" data-bs-toggle="modal"
                                                data-bs-target="#tambahDataPenumpang"><i class="fas fa-plus" data-bs-toggle="tooltip"
                                                    title="Tambah Data"></i></button>
                                                {/* <!-- <button class="btn btn-light text-danger btn-circle custShadow2" data-bs-toggle="modal" data-bs-target="#deleteDataPenumpang"><i class="fas fa-trash" data-bs-toggle="tooltip" title="Hapus Data"></i></button> --> */}
                                            </div>
                                            </div>
                                            <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover dataTable nowrap" width="100%">
                                                <thead>
                                                    <tr>
                                                    <th class="cb">
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" class="selectAll" />
                                                        <label for="selectAll"></label>
                                                        </span>
                                                    </th>
                                                    <th class="actions">Action</th>
                                                    <th class="nik">NIK Penumpang </th>
                                                    <th class="nama">Nama Penumpang</th>
                                                    <th class="jk">Jenis Kelamin</th>
                                                    <th class="no_hp">No Handphone</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {/* <?php
                                                    $data = $obj->lihatPenumpang();
                                                    $no = 1;
                                                    if ($data->rowCount() > 0) {
                                                    if ($sesLvl == 1) {
                                                        $dis = "";
                                                    } else {
                                                        $dis = "disabled";
                                                    }
                                                    while ($row = $data->fetch(PDO::FETCH_ASSOC)) {
                                                        $nik_penumpang = $row['nik_penumpang'];
                                                        $nama_penumpang = $row['nama_penumpang'];
                                                        $jenis_kelamin_penumpang = $row['jenis_kelamin_penumpang'];
                                                        $no_hp_penumpang = $row['no_hp_penumpang'];
                                                    ?> */}
                                                    {/* <tr> */}
                                                    <td>
                                                        <span class="custom-checkbox">
                                                        <input type="checkbox" id="checkbox1" name="option[]" value="<?php echo $no; ?>" />
                                                        <label for="checkbox1"></label>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="actionBtn" aria-label="Edit">
                                                        <button class="btn btn-success btn-user btn-circle" aria-label="EditModal"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#editDataPenumpang<?php echo $nik_penumpang ?>" value="edit">
                                                            &nbsp;<i class="fa fa-edit fa-sm" data-bs-toggle="tooltip" title="Edit"></i>
                                                        </button>
                                                        </a>
                                                        <a href="#" class="actionBtn" aria-label="Delete">
                                                        <button class="btn btn-danger btn-user btn-circle" aria-label="DeleteModal"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#deleteDataPenumpang<?php echo $nik_penumpang; ?>" value="hapus">
                                                            <i class="fa fa-trash fa-sm" data-bs-toggle="tooltip" title="Delete"></i>
                                                        </button>
                                                        </a>

                                                        {/* <!-- Edit Modal --> */}
                                                        <div id="editDataPenumpang<?php echo $nik_penumpang ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content modal-edit">
                                                            <form role="form" action="editPenumpang.php" method="POST">
                                                                {/* <?php
                                                                    $query = $obj->pilihPenumpang($nik_penumpang);
                                                                    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                                                                    $nik_penumpang2 = $row['nik_penumpang'];
                                                                    $nama_penumpang2 = $row['nama_penumpang'];
                                                                    $jenis_kelamin_penumpang2 = $row['jenis_kelamin_penumpang'];
                                                                    $no_hp_penumpang2 = $row['no_hp_penumpang'];
                                                                    ?> */}
                                                                <div class="modal-header">
                                                                <h4 class="modal-title">Edit Data Penumpang</h4>
                                                                <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                                    data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                                    <i class="fa fa-times fa-sm"></i>
                                                                </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputNik" class="form-label">NIK Penumpang</label>
                                                                    <input type="number" class="form-control form-control-user2" id="inputNik"
                                                                        name="txt_nik_penumpang" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        data-parsley-length="[15,16]" maxlength="16" data-parsley-number="1"
                                                                        placeholder="Ex: 3509030907020006" value="<?php echo $nik_penumpang2 ?>" readonly />
                                                                    </div>
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputNamaPenumpang" class="form-label">Nama Penumpang</label>
                                                                    <input type="text" class="form-control form-control-user2"
                                                                        id="inputNamaPenumpang" name="txt_nama_penumpang" required
                                                                        data-parsley-required-message="Data harus di isi !!!"
                                                                        placeholder="Ex: Budi Santoso" value="<?php echo $nama_penumpang2 ?>" />
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="InputJenisKelamin" class="form-label">Jenis Kelamin</label>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio"
                                                                        name="txt_jenis_kelamin_penumpang" id="Radios1" value="Laki-laki"
                                                                        checked />
                                                                        <label class="form-label2" for="Radios1"><span>Laki-laki</span></label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio"
                                                                        name="txt_jenis_kelamin_penumpang" id="Radios2" value="Perempuan" />
                                                                        <label class="form-label2" for="Radios2"><span>Perempuan</span></label>
                                                                    </div>
                                                                    </div>
                                                                    <div class="col-lg-6 mb-3">
                                                                    <label for="inputNoHp" class="form-label">No Handphone</label>
                                                                    <input type="number" class="form-control form-control-user2"
                                                                        id="inputNoHp" name="txt_no_hp_penumpang" placeholder="Ex: 085808241204"
                                                                        required data-parsley-required-message="Data harus di isi !!!"
                                                                        value="<?php echo $no_hp_penumpang2 ?>" />
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button class="btn btn-secondary roundedBtn" type="button"
                                                                    data-bs-dismiss="modal">Batal</button>
                                                                    <button type="submit" class="btn text-white colorPrimary roundedBtn"
                                                                    name="simpan">Update</button>
                                                                </div>
                                                                </div>
                                                            </form>
                                                            {/* <?php
                                                                    }
                                                                ?> */}
                                                            </div>
                                                        </div>
                                                        </div>

                                                        {/* <!-- Delete Modal --> */}
                                                        <div id="deleteDataPenumpang<?php echo $nik_penumpang; ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                            <form action="">
                                                                <div class="modal-header">
                                                                <h4 class="modal-title">Hapus Penumpang</h4>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                    aria-label="Close" aria-hidden="true"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                <p>Apakah Anda yakin ingin menghapus data ini ?</p>
                                                                <p class="text-warning"><small>Perlu hati-hati karena data akan hilang
                                                                    selamanya !</small></p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                <button class="btn btn-secondary" type="button"
                                                                    data-bs-dismiss="modal">Batal</button>
                                                                <a class="btn btn-danger"
                                                                    href="hapusPenumpang.php?nik_penumpang=<?php echo $nik_penumpang; ?>">Hapus</a>
                                                                </div>
                                                            </form>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </td>
                                                    {/* <td><?php echo $nik_penumpang; ?></td>
                                                    <td><?php echo $nama_penumpang; ?></td>
                                                    <td><?php echo $jenis_kelamin_penumpang; ?></td>
                                                    <td><?php echo $no_hp_penumpang; ?></td>
                                                    </tr>
                                                    <?php
                                                        $no++;
                                                    }
                                                    }
                                                    ?> */}
                                                </tbody>
                                                </table>
                                            </div>
                                            </div>

                                            {/* <!-- Tambah Modal --> */}
                                            <div id="tambahDataPenumpang" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content modal-edit">
                                                <form role="form" action="tambahPenumpang.php" method="POST">
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Data Penumpang</h4>
                                                    <button type="button" class="btn btn-danger btn-circle btn-user2 shadow"
                                                        data-bs-dismiss="modal" aria-label="Close" aria-hidden="true">
                                                        <i class="fa fa-times fa-sm"></i>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputNikPenumpang" class="form-label">NIK Penumpang</label>
                                                        <input type="number" class="form-control form-control-user2" id="inputNikPenumpang"
                                                            name="txt_nik_penumpang" placeholder="Ex: 3509030907020006" required
                                                            data-parsley-required-message="Data harus di isi !!!" data-parsley-length="[15,16]"
                                                            maxlength="16" data-parsley-number="1" />
                                                        </div>
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputNamaPenumpang" class="form-label">Nama Penumpang</label>
                                                        <input type="text" class="form-control form-control-user2" id="inputNamaPenumpang"
                                                            name="txt_nama_penumpang" placeholder="Ex: Budi Santoso" required
                                                            data-parsley-required-message="Data harus di isi !!!" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="InputJenisKelamin" class="form-label">Jenis Kelamin</label>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="txt_jenis_kelamin_penumpang"
                                                            id="Radios1" value="Laki-laki" checked />
                                                            <label class="form-label2" for="Radios1"><span>Laki-laki</span></label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="txt_jenis_kelamin_penumpang"
                                                            id="Radios2" value="Perempuan" />
                                                            <label class="form-label2" for="Radios2"><span>Perempuan</span></label>
                                                        </div>
                                                        </div>
                                                        <div class="col-lg-6 mb-3">
                                                        <label for="inputNoHp" class="form-label">No Handphone</label>
                                                        <input type="number" class="form-control form-control-user2" id="inputNoHp"
                                                            name="txt_no_hp_penumpang" placeholder="Ex: 085808241204" required
                                                            data-parsley-required-message="Data harus di isi !!!" />
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <input type="button" class="btn btn-secondary roundedBtn" data-bs-dismiss="modal"
                                                        value="Cancel" />
                                                        <input type="submit" name="simpan" class="btn colorPrimary text-white roundedBtn"
                                                        value="Simpan" />
                                                    </div>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </body>
        )
    }
}
export default Report;