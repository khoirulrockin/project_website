import React, { Component } from "react";
import { Link } from 'react-router-dom';
import '../assets/css/style.css';
import axios from 'axios';


import login from "../assets/images/register.png";
// import logo from "../assets/images/logoW.png";
// import bus3 from "../assets/images/bus3.png";
// import bus2 from "../assets/images/bus2.png";


class Login extends Component{
    render() {
        return (
            <body class="bg-white">
                <div class="info-data" data-infodata="<?php if(isset($_SESSION['info'])){ echo $_SESSION['info']; } unset($_SESSION['info']); ?>"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="card o-hidden border-0 shadow-lg">
                                <div class="card-body p-0">
                                    <div class="row autoHeight">
                                        <div class="col-lg-4 d-none d-lg-flex justify-content-center colorPrimary">
                                            <div class="banner py-5 position-fixed">
                                                <h3 class="mb">
                                                    System Information Booking <br />
                                                    Studio Access<br /><span>Safe, Easy and Fast</span>
                                                </h3>
                                                <img src={login} alt="login" class="img-fluid" width="376px" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8 p-4 py-3 colorSecondary bg-img">
                                            <div class="logo2">
                                                <a href="#">
                                                    <img src="img/logo.png" alt="" />
                                                </a>
                                            </div>
                                            <div class="panel o-hidden border-0 shadow">
                                                <div class="p-5">
                                                    <div class="judul">
                                                        <h4 class="text-gray-900 mb-5">Login <br /><span>System Information Booking Studio Access</span></h4>
                                                    </div>
                                                    <form class="custom-validation" >
                                                        <div class="mb-3">
                                                            <label for="exampleInputEmail" class="form-label">Email</label>
                                                            <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                                                                name="txt_email" required data-parsley-required-message="Email tidak boleh kosong !!!"
                                                                placeholder="Ex: mkhoirulr97@gmail.com" />
                                                        </div>
                                                        <label for="passInput" class="form-label">Password</label>
                                                        <div class="mb-2 wrapper">
                                                            <input type="password" class="form-control form-control-user" id="passInput"
                                                                name="txt_password"
                                                                required data-parsley-required-message="Kata sandi tidak boleh kosong !!!"
                                                                placeholder="********" data-parsley-length="[8,16]" maxlength="16"
                                                                // data-parsley-length-message="Password harus terdiri dari 8 sampai 16 karakter !!!" value=""
                                                            />
                                                            <span class="eye hidden" id="spanEye">
                                                                <i class="fas fa-eye-slash show-hide" toggle="#passInput" id="iconShowHide"></i>
                                                            </span>
                                                        </div>
                                                        <div class="mb-3 float-start">
                                                            <div class="form-check small">
                                                                <input type="checkbox" class="form-check-input" id="customCheck" name="check_remember"
                                                                    value="1" />
                                                                <label class="form-check-label" for="customCheck">Remember Me</label>
                                                            </div>
                                                        </div>
                                                        <div class="mb-3 float-end">
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="row mb-5">
                                                            <div class="col-6 d-flex justify-content-end">
                                                                <a href="/dashboard" class="btn colorSecondary btn-login text-white btn-block py-2">
                                                                    <span>Login</span>
                                                                </a>
                                                            </div>
                                                            <div class="col-6 d-flex justify-content-start">
                                                                <a href="/register" class="btn btn-daftar btn-block py-2">
                                                                    <span>Register</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="d-block position-fixed markQuestion2">
                                                
                                            </div>
                                            <footer class="d-flex justify-content-center text-center mb-1">
                                                <p class="col-md-4 mb-0 text-muted">&copy; 2022 BS Access, All Rights Reserved</p>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <script src="plugin/js/bootstrap.bundle.min.js"></script>
                <script src="jquery/jquery-3.6.0.min.js"></script>
                <script src="plugin/js/form-validation.init.js"></script>
                <script src="plugin/js/parsley.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.3/dist/sweetalert2.all.min.js"
                    integrity="sha256-+InBGKGbhOQiyCbWrARmIEICqZ8UvYJr/qVhHmlmFpc=" crossorigin="anonymous"></script>
                <script src="plugin/js/custom_SweetAlert2.js"></script> */}
                
            </body>
        )
    }
}
export default Login;