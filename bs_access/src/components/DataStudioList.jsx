import React from "react";

const DataStudioList = (props) => {
    function handleHapusStudio(){
        console.log('hello');
        props.handleHapusStudio(props)
    }
    return (
        <tr>
            <th>{props.namaStudio}</th>
            <th>{props.alamat}</th>
            <th>{props.deskripsi}</th>
            <th>{props.hp}</th>
            <th>{props.harga}</th>
            <th className="text-center">
                <button className="btn btn-danger" onClick={handleHapusStudio}>Hapus</button>
            </th>
        </tr>
    );
}

export default DataStudioList;