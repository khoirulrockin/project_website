import React from "react";

const DataAdminList = (props) => {
    function handleHapusadmin(){
        console.log('hello');
        props.handleHapusadmin(props)
    }
    return (
        <tr>
            <th>{props.username}</th>
            <th>{props.email}</th>
            <th>{props.password}</th>
            <th>{props.hp}</th>
            <th className="text-center">
                <button className="btn btn-danger" onClick={handleHapusadmin}>Hapus</button>
            </th>
        </tr>
    );
}

export default DataAdminList;