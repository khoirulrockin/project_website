import React from "react";

const DataUserList = (props) => {
    function handleHapusUser(){
        console.log('hello');
        props.handleHapusUser(props)
    }
    return (
        <tr>
            <th>{props.username}</th>
            <th>{props.nama}</th>
            <th>{props.address}</th>
            <th>{props.birthday}</th>
            <th>{props.phone}</th>
            <th>{props.password}</th>
            <th className="text-center">
                <button className="btn btn-danger" onClick={handleHapusUser}>Hapus</button>
            </th>
        </tr>
    );
}
export default DataUserList;