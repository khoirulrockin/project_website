import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import logo from './logo.svg';
// import './App.css';
import DataAdmin from "../src/views/DataAdmin";
import DataStudio from "../src/views/DataStudio";
import DataUser from "../src/views/DataUser";
import Index from "../src/views/Index";
import Dashboard from "../src/views/Dashboard";
// import Home from "../src/views/Register/Home";
import Laporan from "./views/Report";
import Pemesanan from "./views/Booking";
import SumberData from "../src/views/SumberData";
import Register from "../src/views/Register";
import Login from "../src/views/Login";
import LandingPage from "../src/views/LandingPage";

import EditCategory from "../src/views/edit/EditCategory";
import EditStudio from "../src/views/edit/EditStudio";
import EditAdmin from "../src/views/edit/EditAdmin";
import EditUser from "../src/views/edit/EditUser";
import EditBooking from "../src/views/edit/EditBooking";

function App() {
  return (
    <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={LandingPage} />
              <Route path='/dashboard' component={Dashboard} />
              <Route path='/sumber-data' component={SumberData} />
              <Route path='/data-studio' component={DataStudio} />
              <Route path='/data-admin' component={DataAdmin} />
              <Route path='/data-user' component={DataUser} />
              <Route path='/data-pemesanan' component={Pemesanan} />
              <Route path='/data-laporan' component={Laporan} />
              <Route path='/register' component={Register} />
              <Route path='/login' component={Login} />
              
              <Route path='/edit-category/:id' component={EditCategory} />
              <Route path='/edit-studio/:id' component={EditStudio} />
              <Route path='/edit-admin/:id' component={EditAdmin} />
              <Route path='/edit-user/:id' component={EditUser} />
              <Route path='/edit-booking/:id' component={EditBooking} />
              
              {/* <Route path='/landing-page' component={LandingPage} /> */}
          </Switch>
        </Router>
    </div>
  );
}

export default App;
