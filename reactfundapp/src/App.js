import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';

import Student from './pages/Student';
import Addstudent from './pages/Addstudent';

function App() {
  return (
    <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={Student}/>
            {/* <Route path="/students" component={ViewStudent} /> */}
            <Route path="/add-student" component={Addstudent} />
            {/* <Route path="/edit-student/:id" component={EditStudent} /> */}

          </Switch>
        </Router>
    </div>
  );
}

export default App;
