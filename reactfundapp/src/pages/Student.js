import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Addstudent from './Addstudent';

class Student extends Component{

    // componentDidMount() {
        
    // }

    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12'>
                        <div className='card'>
                            <div className='card-header'>
                                <h4>Student Data
                                    <Link to ={'add-student'} className='btn btn-primary btn-sm float-end'>Add Student</Link>
                                </h4>
                            </div>
                            <div className='card-body'>
                                <table className='table table-bordered table striped'>
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Course</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Student;